/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SRC_I2NP_GARLIC_CLOVE_H_
#define SRC_I2NP_GARLIC_CLOVE_H_

#include <variant>

#include "src/bytes.h"
#include "src/time.h"

#include "src/crypto/sec_bytes.h"

#include "src/data/blocks/i2np.h"

#include "src/data/i2np/data.h"
#include "src/data/i2np/database_store.h"
#include "src/data/i2np/delivery_status.h"
#include "src/data/i2np/garlic_delivery.h"

namespace tini2p
{
namespace data
{
class GarlicClove
{
  enum struct I2NPPart : std::uint8_t
  {
    Instructions,  //< the garlic kind
    Message,  //< any supported I2NP message
  };

 public:
  enum
  {
    NullID = 0,
    CloveIDLen = 4,
    Expiration = 8,  //< in seconds
    ExpirationLen = 8,
    MinLen =
        GarlicDeliveryInstructions::MinLen + Data::MinLen + CloveIDLen + ExpirationLen + data::Certificate::NullCertLen,
    MaxLen = 65531,  //< max transport message size
  };

  using instructions_t = GarlicDeliveryInstructions;  //< Delivery instructions trait alias
  using i2np_block_t = data::I2NPBlock;  //< I2NP block trait alias
  using i2np_message_v = std::variant<Data::shared_ptr, DatabaseStore::shared_ptr, DeliveryStatus::shared_ptr>;  //< I2NP message variant trait alias
  using clove_id_t = std::uint32_t;  //< Clove ID trait alias
  using expiration_t = std::uint64_t;  //< Expiration trait alias
  using cert_t = data::Certificate;  //< Certificate trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Create a fully initialized GarlicClove
  /// @tparam TMessage I2NP message type
  /// @param instructions Garlic delivery instructions for end-to-end routing
  /// @param message I2NP message to deliver
  template <class TMessagePtr>
  GarlicClove(instructions_t instructions, TMessagePtr msg_ptr)
      : instructions_(std::forward<instructions_t>(instructions)),
        message_(std::forward<TMessagePtr>(msg_ptr)),
        clove_id_(crypto::RandInRange()),
        expiration_(tini2p::time::now_s() + Expiration),
        cert_(cert_t::cert_type_t::NullCert),
        buf_()
  {
    serialize();
  }

  /// @brief Create a GarlicCove from a secure buffer
  explicit GarlicClove(buffer_t buf)
  {
    const exception::Exception ex{"GarlicClove", __func__};

    check_len(buf.size(), ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Create a GarlicClove from a C-like buffer
  GarlicClove(const std::uint8_t* data, const std::size_t len)
  {
    const exception::Exception ex{"GarlicClove", __func__};

    tini2p::check_cbuf(data, len, MinLen, MaxLen, ex);

    buf_.resize(len);
    std::copy_n(data, len, buf_.data());

    deserialize();
  }

  /// @brief Serialize GarlicClove to buffer
  void serialize()
  {
    const exception::Exception ex{"GarlicClove", __func__};

    buf_.resize(size());

    check_params(ex);

    tini2p::BytesWriter<buffer_t> writer(buf_);

    // write GarlicDeliveryInstructions
    instructions_.serialize();
    writer.write_data(instructions_.buffer());

    // add I2NP message data to the I2NP block
    complete_i2np_block(ex);
    writer.write_data(msg_block_.buffer());

    writer.write_bytes(clove_id_, tini2p::Endian::Big);
    writer.write_bytes(expiration_, tini2p::Endian::Big);

    cert_.serialize();
    writer.write_data(cert_.buffer);
  }

  /// @brief Deserialize GarlicClove from buffer
  void deserialize()
  {
    const exception::Exception ex{"GarlicClove", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    // read garlic delivery instructions from buffer.
    read_i2np_part(reader, I2NPPart::Instructions, ex);

    // read I2NP message from buffer
    read_i2np_part(reader, I2NPPart::Message, ex);

    reader.read_bytes(clove_id_, tini2p::Endian::Big);
    reader.read_bytes(expiration_, tini2p::Endian::Big);

    // read cert len to resize the buffer properly
    cert_t::length_t cert_len;
    reader.skip_bytes(cert_t::CertTypeLen);
    reader.read_bytes(cert_len, tini2p::Endian::Big);
    reader.skip_back(cert_t::HeaderLen);
    cert_.buffer.resize(cert_t::HeaderLen + cert_len);

    reader.read_data(cert_.buffer);
    cert_.deserialize();

    // reclaim unused buffer space
    buf_.resize(reader.count());
    buf_.shrink_to_fit();
  }

  /// @brief Get the total size of the GarlicClove
  std::uint32_t size() const
  {
    return instructions_.size() + i2np_block_t::HeaderLen + i2np_block_t::MsgHeaderLen
           + std::visit([](const auto& m) -> std::uint32_t { return m->size(); }, message_) + CloveIDLen + ExpirationLen
           + cert_.size();
  }

  /// @brief Get a const reference to the delivery instructions
  const instructions_t& instructions() const noexcept
  {
    return instructions_;
  }

  /// @brief Get a const reference to the inner I2NP message variant
  const i2np_message_v& message() const noexcept
  {
    return message_;
  }

  /// @brief Get a const reference to the clove ID
  const clove_id_t& id() const noexcept
  {
    return clove_id_;
  }

  /// @brief Get a const reference to the clove expiration
  const expiration_t& expiration() const noexcept
  {
    return expiration_;
  }

  /// @brief Get a const reference to the clove certificate
  const cert_t& cert() const noexcept
  {
    return cert_;
  }

  /// @brief Get a const refernce to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const refernce to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Compare equality with another GarlicClove
  bool operator==(const GarlicClove& oth) const
  {  // attempt constant-time
    const auto& instr_eq = static_cast<std::uint8_t>(instructions_ == oth.instructions_);
    const auto& block_eq = static_cast<std::uint8_t>(msg_block_ == oth.msg_block_);
    const auto& id_eq = static_cast<std::uint8_t>(clove_id_ == oth.clove_id_);
    const auto& exp_eq = static_cast<std::uint8_t>(expiration_ == oth.expiration_);
    const auto& cert_eq = static_cast<std::uint8_t>(cert_ == oth.cert_);
 
    return static_cast<bool>(instr_eq * block_eq * id_eq * exp_eq * cert_eq);
  }

 private:
  void complete_i2np_block(const exception::Exception& ex)
  {  // copy I2NP message to message block
    std::lock_guard<std::mutex> mlg(msg_mutex_);
    std::visit(
        [this, &ex](auto& im) {
          msg_block_.buffer().resize(im->size());
          im->serialize();
          auto& msg_buf = msg_block_.msg_data();
          msg_buf = im->buffer();
          msg_block_.msg_type(message_to_type(ex));
          msg_block_.serialize();
        },
        message_);
  }

  void read_i2np_part(tini2p::BytesReader<buffer_t>& reader, const I2NPPart& part, const exception::Exception& ex)
  {
    if (part == I2NPPart::Instructions)
      {  // read flags to resize, then read instructions and deserialize
        instructions_t::flags_t flags;
        reader.read_bytes(flags);
        instructions_.resize_from_flags(flags);
        reader.skip_back(instructions_t::FlagsLen);
        reader.read_data(instructions_.buffer());
        instructions_.deserialize();
      }
    else if (part == I2NPPart::Message)
      {  // read and deserialize I2NP message
        data::I2NPBlock::size_type i2np_size;

        reader.skip_bytes(i2np_block_t::TypeLen);

        reader.read_bytes(i2np_size, tini2p::Endian::Big);
        reader.skip_back(data::I2NPBlock::HeaderLen);

        i2np_block_t::buffer_t t_buf(data::I2NPBlock::HeaderLen + i2np_size);
        reader.read_data(t_buf);
        msg_block_.from_buffer(std::move(t_buf));

        // deserialize I2NP message, throws on unsupported message types
        type_to_message(msg_block_.msg_type(), msg_block_.msg_data(), ex);
      }
    else
      ex.throw_ex<std::invalid_argument>("invalid I2NP part.");
  }

  // TODO(tini2p): update with additional message types, if more will be used in practice
  void type_to_message(const i2np_block_t::msg_type_t type, const buffer_t& buf, const exception::Exception& ex)
  {
    std::lock_guard<std::mutex> mlg(msg_mutex_);
    if (type == i2np_block_t::msg_type_t::Data)
      {
        if (std::holds_alternative<Data::shared_ptr>(message_))
          std::get<Data::shared_ptr>(message_)->from_buffer(buf);
        else
          message_.emplace<Data::shared_ptr>(new Data(buf.data(), buf.size()));
      }
    else if (type == i2np_block_t::msg_type_t::DatabaseStore)
      {
        if (std::holds_alternative<DatabaseStore::shared_ptr>(message_))
          std::get<DatabaseStore::shared_ptr>(message_)->from_buffer(buf);
        else
          message_.emplace<DatabaseStore::shared_ptr>(new DatabaseStore(buf));
      }
    else if (type == i2np_block_t::msg_type_t::DeliveryStatus)
      {
        if (std::holds_alternative<DeliveryStatus::shared_ptr>(message_))
          std::get<DeliveryStatus::shared_ptr>(message_)->from_buffer(buf);
        else
          message_.emplace<DeliveryStatus::shared_ptr>(new DeliveryStatus(buf));
      }
    else
      ex.throw_ex<std::logic_error>("unsupported message type.");
  }


  // TODO(tini2p): update with additional message types, if more will be used in practice
  i2np_block_t::msg_type_t message_to_type(const exception::Exception& ex) const
  {
    i2np_block_t::msg_type_t type;
    if (std::holds_alternative<Data::shared_ptr>(message_))
      type = i2np_block_t::msg_type_t::Data;
    else if (std::holds_alternative<DatabaseStore::shared_ptr>(message_))
      type = i2np_block_t::msg_type_t::DatabaseStore;
    else if (std::holds_alternative<DeliveryStatus::shared_ptr>(message_))
      type = i2np_block_t::msg_type_t::DeliveryStatus;
    else
      ex.throw_ex<std::logic_error>("invalid message type.");

    return type;
  }

  void check_params(const exception::Exception& ex)
  {
    check_len(buf_.size(), ex);

    if (clove_id_ == NullID)
      ex.throw_ex<std::logic_error>("null clove ID.");
  }

  void check_len(const std::size_t len, const exception::Exception& ex)
  {
    if (len < MinLen || len > MaxLen)
      {
        ex.throw_ex<std::length_error>(
            "invalid message size: " + std::to_string(len) + " min: " + std::to_string(tini2p::under_cast(MinLen))
            + " max: " + std::to_string(tini2p::under_cast(MaxLen)));
      }
  }

  instructions_t instructions_;
  i2np_block_t msg_block_;
  i2np_message_v message_;
  std::mutex msg_mutex_;
  clove_id_t clove_id_;
  expiration_t expiration_;
  cert_t cert_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_I2NP_GARLIC_CLOVE_H_
