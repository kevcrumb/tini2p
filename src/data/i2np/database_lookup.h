/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SRC_I2NP_DATABASE_LOOKUP_H_
#define SRC_I2NP_DATABASE_LOOKUP_H_

#include <mutex>

#include "src/bytes.h"

#include "src/crypto/chacha_poly1305.h"
#include "src/crypto/rand.h"
#include "src/crypto/sec_bytes.h"
#include "src/crypto/sha.h"

namespace tini2p
{
namespace data
{
class DatabaseLookup
{
 public:
  enum
  {
    SearchKeyLen = crypto::Sha256::DigestLen,
    FromKeyLen = crypto::Sha256::DigestLen,
    FlagsLen = 1,
    ExPeersSizeLen = 2,
    ExPeerLen = crypto::Sha256::DigestLen,
    MaxExcludedPeers = 512,  // see spec
    TagsSizeLen = 1,
    ReplyTagLen = 8,  // ECIES session tag length (final value TBD, see prop. 144)
    MinReplyTags = 1,  // if encrypted flag set, see spec
    MaxReplyTags = 32,  // if encrypted flag set, see spec
    MinLen = SearchKeyLen + FromKeyLen + FlagsLen + ExPeersSizeLen,  // min message length w/ all required fields, see spec 
    MaxLen = MinLen + (MaxExcludedPeers * ExPeerLen) + (MaxReplyTags * ReplyTagLen),
  };

  enum struct DeliveryFlags : std::uint8_t
  {
    Direct = 0x00,  // 00000000, see spec
    Tunnel = 0x01,  // 00000001, see spec
  };

  enum struct EncryptionFlags : std::uint8_t
  {
    Unencrypted = 0x00,     // 00000000, see spec
    Encrypted = 0x02,       // 00000010, see spec
  };

  enum struct LookupFlags : std::uint8_t
  {
    Normal = 0x00,    // 00000000, see spec
    LS = 0x04,        // 00000100, see spec
    RI = 0x08,        // 00001000, see spec
    Explore = 0x0c,   // 00001100, see spec
  };

  enum : std::uint8_t
  {
    LookupFlagMask = 0x0C,  // 00001100, see spec. Bitwise-AND w/ flags to get lookup type
    ReservedFlagMask = 0xF0,  // 11110000, see spec. Bitwise-AND w/ flags for validity check
  };

  using search_key_t = crypto::Sha256::digest_t;  //< Search key trait alias
  using from_key_t = crypto::Sha256::digest_t;  //< From key trait alias
  using flags_t = std::uint8_t;  //< Flags trait alias
  using reply_tunnel_id_t = std::uint32_t;  //< Reply tunnel ID trait alias
  using ex_peers_size_t = std::uint16_t;  //< Excluded peer size trait alias
  using ex_peer_key_t = crypto::Sha256::digest_t;  //< Excluded peer key trait alias
  using reply_key_t = crypto::ChaChaPoly1305::Key;  //< Reply key trait alias
  using tags_size_t = std::uint8_t;  //< Tags size trait alias
  using reply_tag_t = crypto::FixedSecBytes<ReplyTagLen>;  //< Reply tag trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  using pointer = DatabaseLookup*;  //< Non-owning pointer trait alias
  using const_pointer = const DatabaseLookup*;  //< Const non-owning pointer trait alias
  using unique_ptr = std::unique_ptr<DatabaseLookup>;  //< Unique pointer trait alias
  using const_unique_ptr = std::unique_ptr<const DatabaseLookup>;  //< Const unique pointer trait alias
  using shared_ptr = std::shared_ptr<DatabaseLookup>;  //< Shared pointer trait alias
  using const_shared_ptr = std::shared_ptr<const DatabaseLookup>;  //< Const shared pointer trait alias

  /// @brief Create an unencrypted DatabaseLookup message w/ direct delivery
  /// @param search_key Search key of the entry to lookup
  DatabaseLookup(search_key_t search_key, from_key_t from_key, LookupFlags lookup_flag = LookupFlags::Normal)
      : search_key_(std::forward<search_key_t>(search_key)),
        from_key_(std::forward<from_key_t>(from_key)),
        reply_tunnel_id_(0),
        buf_(MinLen)
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    check_flags({}, {}, lookup_flag, ex);

    flags_ = tini2p::under_cast(lookup_flag);

    serialize();
  }

  /// @brief Create a DatabaseLookup message from a secure buffer
  DatabaseLookup(buffer_t buf)
  {
    from_buffer(std::forward<buffer_t>(buf));
  }

  /// @brief Create a DatabaseLookup message from a C-like buffer
  /// @param data Pointer to the message buffer
  /// @param len Length of the message buffer
  DatabaseLookup(const std::uint8_t* data, const std::size_t len)
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    tini2p::check_cbuf(data, len, MinLen, MaxLen, ex);

    buf_.resize(len);
    std::copy_n(data, len, buf_.data());

    deserialize();
  }

  /// @brief Serialize DatabaseLookup message to buffer
  void serialize()
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    check_params(ex);

    buf_.resize(size());

    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_data(search_key_);
    writer.write_data(from_key_);
    writer.write_bytes(flags_);

    if (flags_ & tini2p::under_cast(DeliveryFlags::Tunnel))
      writer.write_bytes(reply_tunnel_id_, tini2p::Endian::Big);

    writer.write_bytes(static_cast<ex_peers_size_t>(ex_peers_.size()), tini2p::Endian::Big);
    for (const auto& peer : ex_peers_)
      writer.write_data(peer);

    if (flags_ & tini2p::under_cast(EncryptionFlags::Encrypted))
      {
        writer.write_data(reply_key_);
        writer.write_bytes(static_cast<tags_size_t>(reply_tags_.size()));
        for (const auto& tag : reply_tags_)
          writer.write_data(tag);
      }
  }

  /// @brief Deserialize DatabaseLookup message from buffer
  void deserialize()
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    reader.read_data(search_key_);
    reader.read_data(from_key_);

    reader.read_bytes(flags_);
    if (flags_ & ReservedFlagMask)
      ex.throw_ex<std::logic_error>("reserved flags.");

    if (flags_ & tini2p::under_cast(DeliveryFlags::Tunnel))
      reader.read_bytes(reply_tunnel_id_, tini2p::Endian::Big);

    ex_peers_size_t ex_size;
    reader.read_bytes(ex_size, tini2p::Endian::Big);

    if (ex_size > MaxExcludedPeers)
      ex.throw_ex<std::logic_error>("invalid excluded peers size: " + std::to_string(ex_size));

    std::vector<ex_peer_key_t> ex_peer_keys{};
    for (ex_peers_size_t i = 0; i < ex_size; ++i)
      {
        ex_peer_key_t peer;
        reader.read_data(peer);
        ex_peer_keys.emplace_back(std::move(peer));
      }

    {  // lock excluded peers
      std::lock_guard<std::mutex> pgd(peer_mutex_);
      ex_peers_.swap(ex_peer_keys);
    }  // end-excluded-peers-lock

    if (flags_ & tini2p::under_cast(EncryptionFlags::Encrypted))
    {
      reader.read_data(reply_key_);
      tags_size_t tag_size;
      reader.read_bytes(tag_size);

      if (tag_size > MaxReplyTags)
        ex.throw_ex<std::logic_error>("invalid reply tags size: " + std::to_string(tag_size));

      std::vector<reply_tag_t> rep_tags;
      for (tags_size_t i = 0; i < tag_size; ++i)
        {
          reply_tag_t t_tag;
          reader.read_data(t_tag);
          rep_tags.emplace_back(std::move(t_tag));
        }

      {  // lock reply tags
        std::lock_guard<std::mutex> tgd(tag_mutex_);
        reply_tags_.swap(rep_tags);
      }  // end-reply-tags-lock
    }
  }

  /// @brief Create a DatabaseLookup message from a secure buffer
  void from_buffer(buffer_t buf)
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    const auto buf_len = buf.size();
    if (buf_len < MinLen || buf_len > MaxLen)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid buffer length: " + std::to_string(buf_len) + " min: " + std::to_string(tini2p::under_cast(MinLen))
            + " max: " + std::to_string(tini2p::under_cast(MaxLen)));
      }

    buf_ = std::forward<buffer_t>(buf);
    deserialize();
  }

  /// @brief Get the total size of the DatabaseLookup message
  std::uint32_t size() const noexcept
  {
    const auto search_from_keys_len = crypto::Sha256::DigestLen * 2;
    const auto tun_len =
        static_cast<std::uint8_t>(flags_ & tini2p::under_cast(DeliveryFlags::Tunnel)) * sizeof(reply_tunnel_id_t);
    const auto ex_peer_len = ExPeersSizeLen + (ex_peers_.size() * crypto::Sha256::DigestLen);
    const auto enc_len = static_cast<std::uint8_t>(flags_ & tini2p::under_cast(EncryptionFlags::Encrypted))
                         * (crypto::ChaChaPoly1305::KeyLen + TagsSizeLen + (reply_tags_.size() * ReplyTagLen));

    return search_from_keys_len + FlagsLen + tun_len + ex_peer_len + enc_len;
  }

  /// @brief Get a const reference to the search key
  const search_key_t& search_key() const noexcept
  {
    return search_key_;
  }

  /// @brief Get a const reference to the from key
  const from_key_t& from_key() const noexcept
  {
    return from_key_;
  }

  /// @brief Get a const reference to the flags
  const flags_t& flags() const noexcept
  {
    return flags_;
  }

  /// @brief Get the lookup flags for this message
  /// @detail Used for determining the type of DatabaseLookup (Normal, RI, LS, Explore)
  LookupFlags lookup_flags() const noexcept
  {
    return static_cast<LookupFlags>(flags_ & tini2p::under_cast(LookupFlagMask));
  }

  /// @brief Get a const reference to the reply tunnel ID
  /// @detail Used to identify the reply tunnel for DatabaseLookup responses. Zero indicates a direct response.
  const reply_tunnel_id_t& reply_tunnel_id() const noexcept
  {
    return reply_tunnel_id_;
  }

  /// @brief Get a const reference to the list of excluded peers
  /// @detail Used for excluding peers from list of closest floodfills in a DatabaseSearchReply
  const std::vector<ex_peer_key_t>& excluded_peers() const noexcept
  {
    return ex_peers_;
  }

  /// @brief Get a const reference to the reply key
  /// @detail Used for encrypting DatabaseLookup responses
  const reply_key_t& reply_key() const noexcept
  {
    return reply_key_;
  }

  /// @brief Add an excluded peer
  /// @detail Sets the explore lookup flag if peer key is null, see spec
  /// @param peer Excluded peer key (Identity or Destination hash)
  /// @throw Logic error if maximum excluded peer limit exceeded
  void add_excluded_peer(ex_peer_key_t peer)
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    if (ex_peers_.size() == MaxExcludedPeers)
      ex.throw_ex<std::logic_error>("exceeds max excluded peers.");

    if (peer.is_zero())
      flags_ |= tini2p::under_cast(LookupFlags::Explore);

    ex_peers_.emplace_back(std::forward<ex_peer_key_t>(peer));
  }

  /// @brief Set encryption reply key and session tag(s)
  /// @detail Sets the encryption flag
  /// @param key Reply session key
  /// @param tags Reply session tags
  /// @throw Invalid argument for null reply key, null reply tag(s), and/or invalid amount of reply tags
  void set_encrypted_data(reply_key_t key, std::vector<reply_tag_t> tags)
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    if (key.buffer().is_zero())
      ex.throw_ex<std::invalid_argument>("null reply key.");

    const auto tags_len = tags.size();
    if (tags_len < MinReplyTags || tags_len > MaxReplyTags)
      ex.throw_ex<std::invalid_argument>("invalid tags size: " + std::to_string(tags_len));

    for (const auto& tag : tags)
      {
        if (tag.is_zero())
          ex.throw_ex<std::invalid_argument>("null reply tag.");
      }

    flags_ |= tini2p::under_cast(EncryptionFlags::Encrypted);
    reply_key_ = std::forward<reply_key_t>(key);
    reply_tags_ = std::forward<std::vector<reply_tag_t>>(tags);
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another DatabaseLookup message
  bool operator==(const DatabaseLookup& oth) const
  {  // attempt constant-time comparison
    const auto search_eq = static_cast<std::uint8_t>(search_key_ == oth.search_key_);
    const auto from_eq = static_cast<std::uint8_t>(from_key_ == oth.from_key_);
    const auto flags_eq = static_cast<std::uint8_t>(flags_ == oth.flags_);
    const auto rep_tun_eq = static_cast<std::uint8_t>(reply_tunnel_id_ == oth.reply_tunnel_id_);
    const auto ex_peers_eq = static_cast<std::uint8_t>(ex_peers_ == oth.ex_peers_);
    const auto rep_key_eq = static_cast<std::uint8_t>(reply_key_ == oth.reply_key_);
    const auto rep_tags_eq = static_cast<std::uint8_t>(reply_tags_ == oth.reply_tags_);

    return static_cast<bool>(search_eq * from_eq * flags_eq * rep_tun_eq * ex_peers_eq * rep_key_eq * rep_tags_eq);
  }

 private:
  void check_params(const exception::Exception& ex)
  {
    if (search_key_.is_zero() || from_key_.is_zero())
      ex.throw_ex<std::invalid_argument>("null search and/or from key(s).");

    if (flags_ & ReservedFlagMask)
      ex.throw_ex<std::logic_error>("reserved flags: " + std::to_string(flags_));

    if (ex_peers_.size() > MaxExcludedPeers)
      ex.throw_ex<std::logic_error>("invalid excluded peers size: " + std::to_string(ex_peers_.size()));

    if (reply_tags_.size() > MaxReplyTags)
      ex.throw_ex<std::logic_error>("invalid reply tags size: " + std::to_string(reply_tags_.size()));
  }

  void check_flags(
      const DeliveryFlags& dlv_flag = DeliveryFlags::Direct,
      const EncryptionFlags& enc_flag = EncryptionFlags::Unencrypted,
      const LookupFlags& lookup_flag = LookupFlags::Normal,
      const exception::Exception& ex = {"DatabaseLookup", "check_flags"})
  {
    switch(dlv_flag)
    {
      case DeliveryFlags::Direct:
      case DeliveryFlags::Tunnel:
        break;
      default:
        ex.throw_ex<std::invalid_argument>("invalid delivery flag: " + std::to_string(tini2p::under_cast(dlv_flag)));
    }

    switch(enc_flag)
    {
      case EncryptionFlags::Unencrypted:
      case EncryptionFlags::Encrypted:
        break;
      default:
        ex.throw_ex<std::invalid_argument>("invalid encryption flag: " + std::to_string(tini2p::under_cast(enc_flag)));
    }

    switch(lookup_flag)
    {
      case LookupFlags::Normal:
      case LookupFlags::LS:
      case LookupFlags::RI:
      case LookupFlags::Explore:
        break;
      default:
        ex.throw_ex<std::invalid_argument>("invalid lookup flag: " + std::to_string(tini2p::under_cast(lookup_flag)));
    }
  }

  search_key_t search_key_;
  from_key_t from_key_;
  flags_t flags_;
  reply_tunnel_id_t reply_tunnel_id_;
  std::vector<ex_peer_key_t> ex_peers_;
  std::mutex peer_mutex_;
  reply_key_t reply_key_;
  std::vector<reply_tag_t> reply_tags_;
  std::mutex tag_mutex_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_I2NP_DATABASE_LOOKUP_H_
