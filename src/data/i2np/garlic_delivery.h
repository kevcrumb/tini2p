/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SRC_I2NP_GARLIC_DELIVERY_H_
#define SRC_I2NP_GARLIC_DELIVERY_H_

#include "src/bytes.h"

#include "src/crypto/sec_bytes.h"
#include "src/crypto/sha.h"

namespace tini2p
{
namespace data
{
class GarlicDeliveryInstructions
{
 public:
  enum struct DeliveryFlags : std::uint8_t
  {
    Local = 0x00,        //< 00000000, see spec
    Destination = 0x20,  //< 00100000, bit 5 set (0x01), see spec
    Router = 0x40,       //< 01000000, bit 6 set (0x02), see spec
    Tunnel = 0x60,       //< 01100000, bits 5 & 6 set (0x03), see spec
  };

  enum struct UnusedFlags : std::uint8_t
  {
    Encrypted = 0x80,    //< 10000000, unimplemented, never set, see spec
    Unencrypted = 0x00,  //< 00000000, see spec
    Delay = 0x10,        //< 00010000, bit 4 set (not set in practice), see spec
  };

  enum
  {
    FlagsLen = 1,
    HashLen = crypto::Sha256::DigestLen,
    TunnelIDLen = 4,
    DelayLen = 4,
    MinLen = FlagsLen,
    MaxLen = FlagsLen + HashLen + TunnelIDLen + DelayLen,
    ReservedMask = 0x0F,  //< 00001111, reserved bits, bitwise-AND to check, see spec
    DeliveryMask = 0x60,  //< 01100000, delivery bits, bitwise-AND to check, see spec
  };

  using flags_t = std::uint8_t;  //< Flags trait alias
  using hash_t = crypto::Sha256::digest_t; //< ToHash trait alias
  using tunnel_id_t = std::uint32_t;  //< Tunnel ID trait alias
  using delay_t = std::uint32_t;  //< Delay trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor, creates local delivery GarlicDeliveryInstructions
  GarlicDeliveryInstructions() : flags_(), hash_(), tunnel_id_(), delay_()
  {
    serialize();
  }

  /// @brief Fully initializing ctor
  /// @details Provide delivery flags, "to hash", and tunnel ID (if tunnel delivery)
  /// @param flags Delivery flags for determining style of delivery (local, destination, router, tunnel)
  /// @param hash To hash of the endpoint to deliver the garlic message
  /// @param tunnel_id Optional reply tunnel ID (must be set for tunnel delivery)
  /// @throw Logic error if local delivery flag set (use default ctor instead)
  /// @throw Logic error if tunnel delivery flag set, and tunnel ID is zero
  GarlicDeliveryInstructions(const DeliveryFlags& flags, hash_t hash, const tunnel_id_t tunnel_id = 0)
      : flags_(tini2p::under_cast(flags)), hash_(std::forward<hash_t>(hash)), tunnel_id_(tunnel_id), delay_()
  {
    const exception::Exception ex{"GarlicDeliveryInstructions", __func__};

    if (flags == DeliveryFlags::Local)
      ex.throw_ex<std::logic_error>("local delivery set with to hash, use default ctor.");

    if (flags == DeliveryFlags::Tunnel && tunnel_id == 0)
      ex.throw_ex<std::logic_error>("must set tunnel ID for tunnel delivery.");

    if (hash_.is_zero())
      ex.throw_ex<std::invalid_argument>("null `to hash`.");

    serialize();
  }

  /// @brief Create GarlicDeliveryInstructions from a secure buffer
  explicit GarlicDeliveryInstructions(buffer_t buf) : flags_(), hash_(), tunnel_id_(), delay_()
  {
    from_buffer(std::forward<buffer_t>(buf));
  }

  /// @brief Create GarlicDeliveryInstructions from a C-like buffer
  GarlicDeliveryInstructions(const std::uint8_t* data, const std::size_t len)
      : flags_(), hash_(), tunnel_id_(), delay_()
  {
    const exception::Exception ex{"GarlicDeliveryInstructions", __func__};

    tini2p::check_cbuf(data, len, MinLen, MaxLen, ex);

    buf_.resize(len);
    std::copy_n(data, len, buf_.data());

    deserialize();
  };

  /// @brief Serialize GarlicDeliveryInstructions to buffer
  void serialize()
  {
    buf_.resize(size());

    tini2p::BytesWriter<buffer_t> writer(buf_);
    writer.write_bytes(flags_);

    const auto flags = flags_ & DeliveryMask;
    if (flags != tini2p::under_cast(DeliveryFlags::Local))
      writer.write_data(hash_);

    if (flags == tini2p::under_cast(DeliveryFlags::Tunnel))
      writer.write_bytes(tunnel_id_);

    // should never be used, but just in case
    if (flags_ & tini2p::under_cast(UnusedFlags::Delay))
      writer.write_bytes(delay_);
  }

  /// @brief Deserialize GarlicDeliveryInstructions from buffer
  void deserialize()
  {
    const exception::Exception ex{"GarlicDeliveryInstructions", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    reader.read_bytes(flags_);

    check_flags(flags_, ex);

    const auto delivery_flags = flags_ & DeliveryMask;
    if (delivery_flags != tini2p::under_cast(DeliveryFlags::Local))
      reader.read_data(hash_);

    if (delivery_flags == tini2p::under_cast(DeliveryFlags::Tunnel))
      reader.read_bytes(tunnel_id_);

    if (flags_ & tini2p::under_cast(UnusedFlags::Delay))
      reader.read_bytes(delay_);

    buf_.resize(reader.count());
  }

  /// @brief Create GarlicDeliveryInstructions from buffer
  void from_buffer(buffer_t buf)
  {
    const exception::Exception ex{"GarlicDeliveryInstructions", __func__};

    const auto& buf_len = buf.size();
    if (buf_len < MinLen || buf_len > MaxLen)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid length: " + std::to_string(buf_len) + " min: " + std::to_string(tini2p::under_cast(MinLen))
            + " max: " + std::to_string(tini2p::under_cast(MaxLen)));
      }

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Resize the buffer based on the flags present
  void resize_from_flags(const flags_t& flags)
  {
    const exception::Exception ex{"GarlicDeliveryInstructions", __func__};

    check_flags(flags, ex);

    flags_ = flags;
    buf_.resize(size());
  }

  /// @brief Get the total size of the GarlicDeliveryInstructions
  std::uint8_t size() const noexcept
  {  // attempt close to constant-time size calculation
    const auto delivery_flag = flags_ & DeliveryMask;
    const auto hash_size =
        static_cast<std::uint8_t>(delivery_flag != tini2p::under_cast(DeliveryFlags::Local)) * HashLen;
    const auto tun_id_size =
        static_cast<std::uint8_t>(delivery_flag == tini2p::under_cast(DeliveryFlags::Tunnel)) * TunnelIDLen;
    const auto delay_size =
        static_cast<std::uint8_t>((flags_ & tini2p::under_cast(UnusedFlags::Delay)) != 0) * DelayLen;

    return FlagsLen + hash_size + tun_id_size + delay_size;
  }

  /// @brief Get a const reference to the flags
  const flags_t& flags() const noexcept
  {
    return flags_;
  }

  /// @brief Get a const reference to the "to hash"
  const hash_t& to_hash() const noexcept
  {
    return hash_;
  }

  /// @brief Get a const reference to the reply tunnel ID
  const tunnel_id_t& tunnel_id() const noexcept
  {
    return tunnel_id_;
  }

  /// @brief Get a const reference to the delay
  const delay_t& delay() const noexcept
  {
    return delay_;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another GarlicDeliveryInstructions
  bool operator==(const GarlicDeliveryInstructions& oth) const
  {  // attempt constant time
    const auto flags_eq = static_cast<std::uint8_t>(flags_ == oth.flags_);
    const auto hash_eq = static_cast<std::uint8_t>(hash_ == oth.hash_);
    const auto tunnel_eq = static_cast<std::uint8_t>(tunnel_id_ == oth.tunnel_id_);
    const auto delay_eq = static_cast<std::uint8_t>(delay_ == oth.delay_);

    return static_cast<bool>(flags_eq * hash_eq * tunnel_eq * delay_eq);
  }

 private:
  void check_flags(const flags_t& flags, const exception::Exception& ex)
  {
    if (flags & tini2p::under_cast(ReservedMask))
      ex.throw_ex<std::logic_error>("invalid use of reserved flags.");

    if (flags & tini2p::under_cast(UnusedFlags::Encrypted))
      ex.throw_ex<std::logic_error>("invalid use of unsupported encrypted flag.");
  }

  flags_t flags_;
  hash_t hash_;
  tunnel_id_t tunnel_id_;
  delay_t delay_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_I2NP_GARLIC_DELIVERY_H_
