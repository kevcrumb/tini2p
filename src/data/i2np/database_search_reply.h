/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SRC_I2NP_DATABASE_SEARCH_REPLY_H_
#define SRC_I2NP_DATABASE_SEARCH_REPLY_H_

#include <mutex>

#include "src/bytes.h"

#include "src/crypto/sec_bytes.h"
#include "src/crypto/sha.h"

namespace tini2p
{
namespace data
{
class DatabaseSearchReply
{
 public:
  enum
  {
    SearchKeyLen = crypto::Sha256::DigestLen,
    PeerKeyLen = crypto::Sha256::DigestLen,
    PeersSizeLen = 1,
    MaxPeers = 255,
    FromKeyLen = crypto::Sha256::DigestLen,
    MinLen = SearchKeyLen + PeersSizeLen + FromKeyLen,
    MaxLen = MinLen + (MaxPeers * PeerKeyLen),
  };

  using search_key_t = crypto::Sha256::digest_t;  //< Search key trait alias
  using peers_size_t = std::uint8_t;  //< Peer size trait alias
  using peer_key_t = crypto::Sha256::digest_t;  //< Peer key trait alias
  using from_key_t = crypto::Sha256::digest_t;  //< From key trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor
  DatabaseSearchReply() : search_key_(), peer_keys_(), from_key_(), buf_() {}

  /// @brief Create a DatabaseSearchReply message
  /// @param search_key Search key hash
  /// @param from_key From key hash
  /// @param peer_keys Container of peer key hashes
  DatabaseSearchReply(search_key_t search_key, from_key_t from_key, std::vector<peer_key_t> peer_keys = {})
      : search_key_(std::forward<search_key_t>(search_key)), from_key_(std::forward<from_key_t>(from_key))
  {
    const exception::Exception ex{"DatabaseSearchReply", __func__};

    if (search_key.is_zero() || from_key.is_zero())
      ex.throw_ex<std::invalid_argument>("null search and/or from key(s).");

    if (peer_keys.size())
      check_peer_keys(peer_keys, ex);

    peer_keys_ = std::forward<std::vector<peer_key_t>>(peer_keys);

    serialize();
  }

  /// @brief Create a DatabaseSearchReply message from a buffer
  explicit DatabaseSearchReply(buffer_t buf)
  {
    const exception::Exception ex{"DatabaseSearchReply", __func__};

    const auto buf_len = buf.size();
    if (buf_len < MinLen || buf_len > MaxLen)
      ex.throw_ex<std::invalid_argument>("invalid buffer length: " + std::to_string(buf_len));

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Create a DatabaseSearchReply message from a C-like buffer
  DatabaseSearchReply(const std::uint8_t* data, const std::size_t len)
  {
    const exception::Exception ex{"DatabaseSearchReply", __func__};

    tini2p::check_cbuf(data, len, MinLen, MaxLen, ex);

    buf_.resize(len);
    std::copy_n(data, len, buf_.data());

    deserialize();
  }

  /// @brief Serialize DatabaseSearchReply message to buffer
  void serialize()
  {
    const exception::Exception ex{"DatabaseSearchReply", __func__};

    buf_.resize(size());

    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_data(search_key_);

    writer.write_bytes(static_cast<peers_size_t>(peer_keys_.size()));

    for (const auto& peer : peer_keys_)
      writer.write_data(peer);

    writer.write_data(from_key_);
  }

  /// @brief Deserialize DatabaseSearchReply message from buffer
  void deserialize()
  {
    const exception::Exception ex{"DatabaseSearchReply", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    reader.read_data(search_key_);

    if (search_key_.is_zero())
      ex.throw_ex<std::logic_error>("null search key.");

    peers_size_t peers_len;
    reader.read_bytes(peers_len);
    
    std::vector<peer_key_t> peer_keys;
    for (peers_size_t i = 0; i < peers_len; ++i)
      {
        peer_key_t peer;
        reader.read_data(peer);

        if (peer.is_zero())
          ex.throw_ex<std::logic_error>("null peer key.");

        peer_keys.emplace_back(std::move(peer));
      }

    {  // lock peer keys
      std::lock_guard<std::mutex> pgd(peers_mutex_);
      peer_keys_.swap(peer_keys);
    }  // end-peer-keys-lock

    reader.read_data(from_key_);

    if (from_key_.is_zero())
      ex.throw_ex<std::logic_error>("null from key.");
  }

  /// @brief Returns the total size of the DatabaseSearchReply message
  std::uint32_t size() const
  {
    return SearchKeyLen + PeersSizeLen + (peer_keys_.size() * PeerKeyLen) + FromKeyLen;
  }

  /// @brief Get a const reference to the search key hash
  const search_key_t& search_key() const noexcept
  {
    return search_key_;
  }

  /// @brief Get a const reference to the peer key hashes
  const std::vector<peer_key_t>& peer_keys() const noexcept
  {
    return peer_keys_;
  }

  /// @brief Set peer key hashes
  /// @detail Maximum number of peer keys is 255, see spec
  /// @param peer_keys Collection of peer key hashes
  /// @throw Invalid argument on null or excessive peer key(s)
  void peer_keys(std::vector<peer_key_t> peer_keys)
  {
    const exception::Exception ex{"DatabaseSearchReply", __func__};

    check_peer_keys(peer_keys, ex);

    peer_keys_ = std::forward<std::vector<peer_key_t>>(peer_keys);
  }

  /// @brief Get a const reference to the from key hash
  const from_key_t& from_key() const noexcept
  {
    return from_key_;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Compare equality with other DatabaseSearchReply message
  bool operator==(const DatabaseSearchReply& oth) const
  {  // attempt constant-time
    const auto search_eq = static_cast<std::uint8_t>(search_key_ == oth.search_key_);
    const auto peers_eq = static_cast<std::uint8_t>(peer_keys_ == oth.peer_keys_);
    const auto from_eq = static_cast<std::uint8_t>(from_key_ == oth.from_key_);

    return static_cast<bool>(search_eq * peers_eq * from_eq);
  }

 private:
  void check_peer_keys(const std::vector<peer_key_t>& peer_keys, const exception::Exception& ex)
  {
    if (peer_keys.size() > MaxPeers)
      ex.throw_ex<std::invalid_argument>("too many peers: " + std::to_string(peer_keys.size()));

    for (const auto& peer : peer_keys)
      {
        if (peer.is_zero())
          ex.throw_ex<std::invalid_argument>("null peer key.");
      }
  }

  search_key_t search_key_;
  std::vector<peer_key_t> peer_keys_;
  std::mutex peers_mutex_;
  from_key_t from_key_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_I2NP_DATABASE_SEARCH_REPLY_H_
