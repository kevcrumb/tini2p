/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SRC_I2NP_DATA_H_
#define SRC_I2NP_DATA_H_

#include "src/bytes.h"

#include "src/crypto/sec_bytes.h"

#include "src/data/blocks/i2np.h"

namespace tini2p
{
namespace data
{
class Data
{
 public:
  enum
  {
    LengthLen = 4,
    MinLen = LengthLen,
    MaxLen = data::I2NPBlock::MaxMsgLen,
    MaxDataLen = MaxLen - LengthLen,
  };

  using length_t = std::uint32_t;  //< Length trait alias
  using data_t = crypto::SecBytes;  //< Data trait alias
  using buffer_t = crypto::SecBytes; //< Buffer trait alias

  using pointer = Data*;  //< Non-owning pointer trait alias
  using const_pointer = const Data*;  //< Const non-owning pointer trait alias
  using unique_ptr = std::unique_ptr<Data>;  //< Unique pointer trait alias
  using const_unique_ptr = std::unique_ptr<const Data>;  //< Const unique pointer trait alias
  using shared_ptr = std::shared_ptr<Data>;  //< Shared pointer trait alias
  using const_shared_ptr = std::shared_ptr<const Data>;  //< Const shared pointer trait alias

  Data() : length_(), data_(), buf_(LengthLen) {}

  Data(data_t data) : length_(data.size())
  {
    const exception::Exception ex{"I2NP: Data", __func__};

    check_params(ex);

    data_ = std::forward<data_t>(data);
    serialize();
  }

  /// @brief Create a Data message from a C-like buffer
  Data(const std::uint8_t* data, const std::size_t len) : length_(len)
  {
    const exception::Exception ex{"I2NP: Data", __func__};

    tini2p::check_cbuf(data, len, MinLen, MaxLen, ex);

    buf_.resize(len);
    std::copy_n(data, len, buf_.data());

    deserialize();
  }

  /// @brief Serialize the data message to the buffer
  void serialize()
  {
    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_bytes(length_, tini2p::Endian::Big);
    writer.write_data(data_);
  }

  /// @brief Deserialize the data message from the buffer
  void deserialize()
  {
    const exception::Exception ex{"I2NP: Data", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    reader.read_bytes(length_, tini2p::Endian::Big);

    check_params(ex);

    data_.resize(length_);
    reader.read_data(data_);
  }

  void from_buffer(buffer_t buf)
  {
    const exception::Exception ex{"I2NP: Data", __func__};

    const auto& buf_len = buf.size();
    if (buf_len < MinLen || buf_len > MaxLen)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid length: " + std::to_string(buf_len) + " min: " + std::to_string(tini2p::under_cast(MinLen))
            + " max: " + std::to_string(tini2p::under_cast(MaxLen)));
      }

    buf_ = std::forward<buffer_t>(buf);
    deserialize();
  }

  /// @brief Get the total size of the data message
  length_t size() const noexcept
  {
    return LengthLen + length_;
  }

  /// @brief Get the length of the data payload
  const length_t& data_length() const noexcept
  {
    return length_;
  }

  /// @brief Get a const reference to the data payload
  const data_t& data() const noexcept
  {
    return data_;
  }

  /// @brief Set the data payload
  void data(data_t data)
  {
    const exception::Exception ex{"I2NP: Data", __func__};

    length_ = data.size();
    check_params(ex);

    data_ = std::forward<data_t>(data);
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Compare equality with another Data message
  bool operator==(const Data& oth) const
  {
    const auto& len_eq = static_cast<std::uint8_t>(length_ == oth.length_);
    const auto& data_eq = static_cast<std::uint8_t>(data_ == oth.data_);

    return static_cast<bool>(len_eq * data_eq);
  }

 private:
  void check_params(const exception::Exception& ex)
  {
    const auto& buf_len = buf_.size();
    if(buf_len < MinLen || length_ > MaxDataLen)
      {
        ex.throw_ex<std::length_error>(
            "invalid length: buf_len: " + std::to_string(buf_len) + " data_len: " + std::to_string(length_)
            + " min: " + std::to_string(MinLen) + " max: " + std::to_string(tini2p::under_cast(MaxLen)));
      }
  }

  length_t length_;
  data_t data_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_I2NP_DATA_H_
