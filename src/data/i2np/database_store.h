/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SRC_I2NP_DATABASE_STORE_H_
#define SRC_I2NP_DATABASE_STORE_H_

#include <variant>

#include "src/gzip.h"

#include "src/crypto/sec_bytes.h"
#include "src/crypto/sha.h"

#include "src/data/router/info.h"
#include "src/data/router/lease_set.h"

namespace tini2p
{
namespace data
{
/// @class DatabaseStore
/// @brief DatabaseStore message
class DatabaseStore
{
 public:
  enum
  {
    SearchKeyLen = crypto::Sha256::DigestLen,
    TypeLen = 1,
    TokenLen = 4,
    TunnelIDLen = 4,
    GatewayLen = crypto::Sha256::DigestLen,
    HeaderLen = SearchKeyLen + TypeLen + TokenLen,
    ExtHeaderLen = HeaderLen + TunnelIDLen + GatewayLen,
    NullToken = 0,
    TokenMin = 1,
    TokenMax = 4294967295,  // uint32_max
    MinLen = HeaderLen + data::LeaseSet2::MinLen,
    MaxLen = 65505,  // Block::MaxLen - I2NPBlockHeader
  };

  enum struct Type : std::uint8_t
  {
    RouterInfo = 0x00,          //< 00000000, see spec
    LeaseSet = 0x01,            //< 00000001, see spec
    LeaseSet2 = 0x03,           //< 00000011, see spec
    EncryptedLeaseSet2 = 0x05,  //< 00000101, see spec
    MetaLeaseSet2 = 0x07,       //< 00000111, see spec
    Unsupported = 0xFF,         //< 11111111, locally unsupported
  };

  using search_key_t = crypto::Sha256::digest_t;  //< Search key trait alias
  using type_t = Type;  //< Type trait alias
  using reply_token_t = std::uint32_t;  //< Reply token trait alias
  using reply_tunnel_id_t = std::uint32_t;  //< Reply tunnel ID trait alias
  using reply_gateway_t = crypto::Sha256::digest_t;  //< Reply tunnel gateway trait alias
  using data_v = std::variant<data::Info::shared_ptr, data::LeaseSet2::shared_ptr>;  //< Data entry trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  using pointer = DatabaseStore*;  //< Non-owning pointer trait alias
  using const_pointer = const DatabaseStore*;  //< Const non-owning pointer trait alias
  using unique_ptr = std::unique_ptr<DatabaseStore>;  //< Unique pointer trait alias
  using const_unique_ptr = std::unique_ptr<const DatabaseStore>;  //< Const unique pointer trait alias
  using shared_ptr = std::shared_ptr<DatabaseStore>;  //< Shared pointer trait alias
  using const_shared_ptr = std::shared_ptr<const DatabaseStore>;  //< Const shared pointer trait alias

  /// @brief Default ctor
  DatabaseStore()
      : search_key_(), reply_token_(NullToken), reply_tunnel_id_(), reply_gateway_(), data_(), buf_(), reachable_(false)
  {
  }

  /// @brief Create a DatabaseStore message w/ no reply token
  /// @tparam TEntry Entry type to store
  /// @param entry Entry to store in the database
  template <
      class TEntryPtr,
      typename = std::enable_if_t<
          std::is_same<TEntryPtr, data::Info::shared_ptr>::value
          || std::is_same<TEntryPtr, data::LeaseSet2::shared_ptr>::value>>
  explicit DatabaseStore(TEntryPtr entry)
      : reply_token_(NullToken), reply_tunnel_id_(), reply_gateway_(), buf_(), reachable_(true)
  {
    const exception::Exception ex{"I2NP: DatabaseStore", __func__};

    if (!entry)
      ex.throw_ex<std::invalid_argument>("null entry.");

    type_ = entry_to_type(*entry);
    data_.emplace<TEntryPtr>(std::forward<TEntryPtr>(entry));

    serialize();
  }

  /// @brief Create a DatabaseStore message w/ reply fields set
  /// @tparam TEntry Entry type to store
  /// @param entry Entry to store in the database
  /// @param reply_id Tunnel reply ID
  template <
      class TEntryPtr,
      typename = std::enable_if_t<
          std::is_same<TEntryPtr, data::Info::shared_ptr>::value
          || std::is_same<TEntryPtr, data::LeaseSet2::shared_ptr>::value>>
  DatabaseStore(TEntryPtr entry, reply_tunnel_id_t reply_id, reply_gateway_t reply_gw)
      : reply_token_(crypto::RandInRange(tini2p::under_cast(TokenMin), tini2p::under_cast(TokenMax))),
        reply_tunnel_id_(std::forward<reply_tunnel_id_t>(reply_id)),
        reply_gateway_(std::forward<reply_gateway_t>(reply_gw)),
        buf_(),
        reachable_(true)
  {
    const exception::Exception ex{"I2NP: DatabaseStore", __func__};

    if (!entry)
      ex.throw_ex<std::invalid_argument>("null entry.");

    type_ = entry_to_type(*entry);
    data_.emplace<TEntryPtr>(std::forward<TEntryPtr>(entry));

    serialize();
  }

  /// @brief Create a DatabaseStore message from a buffer
  explicit DatabaseStore(buffer_t buf)
  {
    from_buffer(std::forward<buffer_t>(buf));
  }

  /// @brief Create a DatabaseStore message from a C-like buffer
  DatabaseStore(const std::uint8_t* data, const std::size_t len)
  {
    const exception::Exception ex{"I2NP: DatabaseStore", __func__};

    tini2p::check_cbuf(data, len, MinLen, MaxLen, ex);

    buf_.resize(len);
    std::copy_n(data, len, buf_.data());

    deserialize();
  }

  /// @brief Serialize message to buffer
  void serialize()
  {
    const exception::Exception ex{"I2NP: DatabaseStore", __func__};

    buf_.resize(size());

    tini2p::BytesWriter<buffer_t> writer(buf_);

    {  // lock data
      std::lock_guard<std::mutex> dlg(data_mutex_);
      // serialize the entry data and update the search key
      search_key_ = std::visit(
          [](auto& d) {
            d->serialize();
            return d->hash();
          },
          data_);

      // write message header information
      writer.write_data(search_key_);
      writer.write_bytes(type_);
      writer.write_bytes(reply_token_, tini2p::Endian::Big);

      // write reply tunnel + gateway if reply token set
      if (reply_token_ > NullToken)
        {
          writer.write_bytes(reply_tunnel_id_, tini2p::Endian::Big);
          writer.write_data(reply_gateway_);
        }

      // write entry data
      std::visit(
          [&writer](auto& d) {
            using entry_t = std::decay_t<decltype(d)>;

            if (std::is_same<entry_t, data::Info::shared_ptr>::value)
              {  //  Gzip compress the RouterInfo
                const auto& d_buf = d->buffer();
                typename entry_t::element_type::buffer_t t_buf;

                tini2p::Gzip::Compress(d_buf.data(), d_buf.size(), t_buf);
                writer.write_data(t_buf);
              }
            else
              writer.write_data(d->buffer());  // write non-RouterInfo entry
          },
          data_);
    }  // end-lock-data

    // adjust for potential space freed by compressed RouterInfo
    buf_.resize(writer.count());
    buf_.shrink_to_fit();
    reachable_ = true;
  }

  /// @brief Deserialize message to buffer
  void deserialize()
  {
    const exception::Exception ex{"I2NP: DatabaseStore", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    reader.read_data(search_key_);
    reader.read_bytes(type_);
    reader.read_bytes(reply_token_, tini2p::Endian::Big);

    if (reply_token_ > NullToken)
      {
        reader.read_bytes(reply_tunnel_id_, tini2p::Endian::Big);
        reader.read_data(reply_gateway_);
      }

    {  // lock data
      std::lock_guard<std::mutex> dlg(data_mutex_);
      if (type_ == type_t::RouterInfo)
        {
          // decompress Gzip'ed RouterInfo, and update entry data w/ deserialized RouterInfo
          crypto::SecBytes decomp_buf;
          tini2p::Gzip::Decompress(buf_.data() + reader.count(), reader.gcount(), decomp_buf);
          data_.emplace<Info::shared_ptr>(new Info(decomp_buf));
        }
      else if (type_ == type_t::LeaseSet2)
        data_.emplace<LeaseSet2::shared_ptr>(new LeaseSet2(buf_.data() + reader.count(), reader.gcount()));
      else
        reachable_ = false;
    }  // end-lock-data
  }

  /// @brief Create a DatabaseStore message from a secure buffer
  void from_buffer(buffer_t buf)
  {
    const exception::Exception ex{"I2NP: DatabaseStore", __func__};

    const auto& buf_len = buf.size();
    if (buf_len < MinLen || buf_len > MaxLen)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid length: " + std::to_string(buf_len) + " min: " + std::to_string(tini2p::under_cast(MinLen))
            + " max: " + std::to_string(tini2p::under_cast(MaxLen)));
      }

    buf_ = std::forward<buffer_t>(buf);
    deserialize();
  }

  /// @brief Verify the entry data
  bool Verify() const
  {
    return std::visit([](const auto& d) { return d->Verify(); }, data_);
  }

  /// @brief Get the size of the DatabaseStore message
  std::uint32_t size() const
  {
    return (reply_token_ > tini2p::under_cast(NullToken) ? HeaderLen : ExtHeaderLen)
           + std::visit([](const auto& d) -> std::uint32_t { return d->size(); }, data_);
  }

  /// @brief Set new entry data
  /// @detail Resets search key to entry hash, and type to corresponding entry type
  /// @brief Create a DatabaseStore message w/ no reply token
  /// @tparam TEntry Entry type to store
  /// @param entry Entry to store in the database
  template <
      class TEntryPtr,
      typename = std::enable_if_t<
          std::is_same<TEntryPtr, data::Info::shared_ptr>::value
          || std::is_same<TEntryPtr, data::LeaseSet2::shared_ptr>::value>>
  void entry_data(TEntryPtr entry)
  {
    const exception::Exception ex{"I2NP: DatabaseStore", __func__};

    if (!entry)
      ex.throw_ex<std::invalid_argument>("null entry data.");

    search_key_ = entry->hash();
    type_ = entry_to_type(*entry);
    data_ = entry;
  }

  /// @brief Get a const reference to the search key
  const search_key_t& search_key() const noexcept
  {
    return search_key_;
  }

  /// @brief Get a const reference to the entry data
  const data_v& entry_data() const noexcept
  {
    return data_;
  }

  /// @brief Get a const reference to the store type
  const type_t& type() const noexcept
  {
    return type_;
  }

  /// @brief Get a const reference to the reply token
  const reply_token_t& reply_token() const noexcept
  {
    return reply_token_;
  }

  /// @brief Get a const reference to the reply tunnel ID
  const reply_tunnel_id_t& reply_tunnel_id() const noexcept
  {
    return reply_tunnel_id_;
  }

  /// @brief Get a const reference to the reply gateway
  const reply_gateway_t& reply_gateway() const noexcept
  {
    return reply_gateway_;
  }

  /// @brief Set the reply token, tunnel ID and gateway
  /// @detail Sets reply token to random value in valid range
  /// @param tunnel_id Reply tunnel ID
  /// @param gateway Reply tunnel gateway router hash
  /// @throw Invalid argument for null tunnel ID or gateway
  void reply(reply_tunnel_id_t tunnel_id, reply_gateway_t gateway)
  {
    const exception::Exception ex{"I2NP: DatabaseStore", __func__};

    if (tunnel_id == 0)
      ex.throw_ex<std::invalid_argument>("null reply tunnel ID.");

    if (gateway.is_zero())
      ex.throw_ex<std::invalid_argument>("null reply gateway.");

    reply_token_ = crypto::RandInRange(TokenMin, TokenMax);
    reply_tunnel_id_ = tunnel_id;
    reply_gateway_ = std::forward<reply_gateway_t>(gateway);
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Convert entry type to DatabaseStore message type
  /// @tparam TEntry Entry type to store
  /// @param entry Entry to store in the database
  template <
      class TEntry,
      typename =
          std::enable_if_t<std::is_same<TEntry, data::Info>::value || std::is_same<TEntry, data::LeaseSet2>::value>>
  type_t entry_to_type(const TEntry& entry) const noexcept
  {
    type_t type;
    if (std::is_same<TEntry, data::Info>::value)
      type = type_t::RouterInfo;
    else if (std::is_same<TEntry, data::LeaseSet2>::value)
      type = type_t::LeaseSet2;
    else
      type = type_t::Unsupported;

    return type;
  }

  /// @brief Returns whether the entry is a supported type
  /// @detail Unsupported types will be relayed, but are unused otherwise
  bool locally_reachable() const noexcept
  {
    return reachable_;
  }

  /// Equality comparison with another DatabaseStore message
  bool operator==(const DatabaseStore& oth) const
  {  // attempt constant-time
    const auto& search_eq = static_cast<std::uint8_t>(search_key_ == oth.search_key_);
    const auto& type_eq = static_cast<std::uint8_t>(type_ == oth.type_);
    const auto& token_eq = static_cast<std::uint8_t>(reply_token_ == oth.reply_token_);
    const auto& id_eq = static_cast<std::uint8_t>(reply_tunnel_id_ == oth.reply_tunnel_id_);
    const auto& gateway_eq = static_cast<std::uint8_t>(reply_gateway_ == oth.reply_gateway_);
    const auto& data_eq = static_cast<std::uint8_t>(data_ == oth.data_);
    const auto& reachable_eq = static_cast<std::uint8_t>(reachable_ == oth.reachable_);

    return static_cast<bool>(search_eq * type_eq * token_eq * id_eq * gateway_eq * data_eq * reachable_eq);
  }

 private:
  search_key_t search_key_;
  type_t type_;
  reply_token_t reply_token_;
  reply_tunnel_id_t reply_tunnel_id_;
  reply_gateway_t reply_gateway_;
  data_v data_;
  std::mutex data_mutex_;
  buffer_t buf_;
  bool reachable_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_I2NP_DATABASE_STORE_H_
