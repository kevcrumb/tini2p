/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SRC_I2NP_DATABASE_DELIVERY_STATUS_H_
#define SRC_I2NP_DATABASE_DELIVERY_STATUS_H_

#include "src/bytes.h"
#include "src/time.h"

#include "src/crypto/sec_bytes.h"

namespace tini2p
{
namespace data
{
/// @class DeliveryStatus
class DeliveryStatus
{
 public:
  enum
  {
    MessageIDLen = 4,
    TimestampLen = 8,
    MessageLen = MessageIDLen + TimestampLen,
  };

  using message_id_t = std::uint32_t;  //< Message ID trait alias
  using timestamp_t = std::uint64_t;  //< Timestamp trait alias
  using buffer_t = crypto::FixedSecBytes<MessageLen>;  //< Buffer trait alias

  using pointer = DeliveryStatus*;  //< Non-owning pointer trait alias
  using const_pointer = const DeliveryStatus*;  //< Const non-owning pointer trait alias
  using unique_ptr = std::unique_ptr<DeliveryStatus>;  //< Unique pointer trait alias
  using const_unique_ptr = std::unique_ptr<const DeliveryStatus>;  //< Const unique pointer trait alias
  using shared_ptr = std::shared_ptr<DeliveryStatus>;  //< Shared pointer trait alias
  using const_shared_ptr = std::shared_ptr<const DeliveryStatus>;  //< Const shared pointer trait alias

  DeliveryStatus(message_id_t message_id, timestamp_t time = tini2p::time::now_s())
      : message_id_(message_id), time_(time), buf_()
  {
    serialize();
  }

  explicit DeliveryStatus(buffer_t buf) : buf_(std::forward<buffer_t>(buf))
  {
    deserialize();
  }

  explicit DeliveryStatus(const crypto::SecBytes& buf)
  {
    from_buffer(buf);
  }

  DeliveryStatus(const std::uint8_t* data, const std::size_t len)
  {
    const exception::Exception ex{"DeliveryStatus", __func__};

    tini2p::check_cbuf(data, len, MessageLen, MessageLen, ex);

    std::copy_n(data, len, buf_.data());

    deserialize();
  }

  void serialize()
  {
    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_bytes(message_id_, tini2p::Endian::Big);
    writer.write_bytes(time_, tini2p::Endian::Big);
  }

  void deserialize()
  {
    tini2p::BytesReader<buffer_t> reader(buf_);

    reader.read_bytes(message_id_, tini2p::Endian::Big);
    reader.read_bytes(time_, tini2p::Endian::Big);
  }

  /// @brief Create a DeliveryStatus message from a secure buffer
  void from_buffer(const crypto::SecBytes& buf)
  {
    const exception::Exception ex{"DeliveryStatus", __func__};

    const auto& buf_len = buf.size();
    if (buf_len != MessageLen)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid message size: " + std::to_string(buf_len)
            + " expected: " + std::to_string(tini2p::under_cast(MessageLen)));
      }

    std::copy_n(buf.data(), buf_len, buf_.data());
    deserialize();
  }

  std::uint8_t size() const noexcept
  {
    return MessageLen;
  }

  const message_id_t& message_id() const noexcept
  {
    return message_id_;
  }

  const timestamp_t& timestamp() const noexcept
  {
    return time_;
  }

  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another DeliveryStatus message
  bool operator==(const DeliveryStatus& oth) const
  {  // attempt constant-time
    const auto msg_id_eq = static_cast<std::uint8_t>(message_id_ == oth.message_id_);
    const auto time_eq = static_cast<std::uint8_t>(time_ == oth.time_);

    return static_cast<bool>(msg_id_eq * time_eq);
  }

 private:
  message_id_t message_id_;
  timestamp_t time_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_I2NP_DATABASE_DELIVERY_STATUS_H_
