/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SRC_DATA_ROUTER_MAPPING_H_
#define SRC_DATA_ROUTER_MAPPING_H_

#include <mutex>
#include <map>

#include "src/bytes.h"
#include "src/crypto/sec_bytes.h"
#include "src/exception/exception.h"

namespace tini2p
{
namespace data
{
/// @brief Class for storing/processing I2P mappings
class Mapping
{
  enum struct CheckLen : bool
  {
    NoCheck,
    Check,
  };

 public:
  enum : std::uint16_t
  {
    KeyPos = 0,  //< map key index
    ValPos = 1,  //< map value index
    KVDelimLen = 1,
    TailDelimLen = 1,
    DelimLen = KVDelimLen + TailDelimLen,
    KVSizeLen = 1,
    EntrySizeLen = 2,
    MinKVLen = 1,
    MaxKVLen = 255,
    MinEntryLen = EntrySizeLen + MinKVLen + DelimLen,  //< 1-byte key, empty value, delimiters, see spec
    MaxEntryLen = DelimLen + MinKVLen + MaxKVLen,
    SizeOffset = 0,
    SizeLen = 2,
    KeySizeOffset = SizeOffset + SizeLen,
    MinLen = SizeLen + MinEntryLen,
    // TODO(tini2p): will we ever need more?
    MaxLen = SizeLen + 32767,  //< ~32 kB, arbitrary max
  };

  using size_type = std::uint16_t;  //< Size trait alias
  using key_t = crypto::SecBytes;  //< Key trait alias
  using value_t = crypto::SecBytes;  //< Value trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor to create an empty Mapping
  Mapping() : buf_(tini2p::under_cast(MinLen))
  {
    serialize();
  }

  /// @brief Copy-ctor
  explicit Mapping(const Mapping& oth) : kv_(oth.kv_), buf_(oth.buf_) {}

  /// @brief Move-ctor
  explicit Mapping(Mapping&& oth) : kv_(std::move(oth.kv_)), buf_(std::move(oth.buf_)) {}

  /// @brief Deserializing ctor to create a Mapping from a secure buffer
  explicit Mapping(buffer_t buf) : buf_(std::forward<buffer_t>(buf))
  {
    deserialize();
  }

  /// @brief Deserializing ctor to create Mapping from a C-like buffer
  Mapping(const std::uint8_t* data, const std::size_t len)
  {
    tini2p::check_cbuf(data, len, MinLen, MaxLen, {"Mapping", __func__});

    buf_.resize(len);
    std::copy_n(data, len, buf_.data());

    deserialize();
  }

  /// @brief Serialize the mapping to buffer
  void serialize()
  {
    const auto& tot_size = size();
    buf_.resize(tot_size);
    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_bytes<size_type>(tot_size - tini2p::under_cast(SizeLen), tini2p::Endian::Big);

    for (const auto& kv : kv_)
      {
        // write key + key-value delimiter
        const auto& key = std::get<KeyPos>(kv);
        writer.write_bytes<std::uint8_t>(key.size());
        writer.write_data(key);
        writer.write_data(kv_delim_);

        // write value + tail delimiter
        const auto& value = std::get<ValPos>(kv);
        writer.write_bytes<std::uint8_t>(value.size());
        writer.write_data(value);
        writer.write_data(tail_delim_);
      }
  }

  /// @brief Deserialize the mapping from buffer
  void deserialize()
  {
    const exception::Exception ex{"Router: Mapping", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    // read the mapping size
    size_type size;
    reader.read_bytes(size, tini2p::Endian::Big);

    if (!size)
      return;

    if (size < MinLen || size + SizeLen > buf_.size())
      ex.throw_ex<std::length_error>("invalid mapping size.");

    std::map<key_t, value_t> kv;
    while (reader.gcount())
      {
        // read the key size
        std::uint8_t key_size;
        reader.read_bytes(key_size);

        // check key size is valid
        check_param_len(key_size, ex);

        // read the key, skip the key-value delim
        key_t key(key_size);
        reader.read_data(key);
        reader.skip_bytes(KVDelimLen);

        // read the value size
        std::uint8_t val_size;
        reader.read_bytes(val_size);

        // check value size is valid
        check_param_len(val_size, ex);

        // read the value, skip the trailing delim
        value_t value(val_size);
        reader.read_data(value);
        reader.skip_bytes(TailDelimLen);

        // add the deserialized entry, no need to check param len (we already did that)
        add_entry(kv, key, value, CheckLen::NoCheck);
      }

    // swap the contents with the temp key-value store
    std::lock_guard<std::mutex> klg(kv_mutex_);
    kv_.swap(kv);
  }

  /// @brief Return the size of the mapping buffer
  size_type size() const noexcept
  {  // attempt constant-time
    size_type total_size = tini2p::under_cast(SizeLen);
    for (const auto& kv : kv_)
      {
        const auto& key = std::get<KeyPos>(kv);
        total_size += static_cast<size_type>(!key.empty()) * entry_size(key, std::get<ValPos>(kv));
      }

    return total_size;
  }

  /// @brief Get the total size of a Mapping entry
  /// @param key Mapping entry key
  /// @param value Mapping entry value
  template <class TKey, class TValue>
  size_type entry_size(const TKey& key, const TValue& value) const
  {
    return key.size() + value.size() + tini2p::under_cast(DelimLen) + tini2p::under_cast(EntrySizeLen);
  }

  /// @brief Get a const reference to the mapping entry at key
  /// @param key Mapping key to search for a value
  /// @return The key's value or an empty value if unfound
  template <class TKey>
  const value_t& entry(const TKey& key) const
  {
    const exception::Exception ex{"Router: Mapping", __func__};

    const auto pos = kv_.find(key_t(reinterpret_cast<const std::uint8_t*>(key.data()), key.size()));

    if (pos == kv_.end())
      ex.throw_ex<std::logic_error>("no entry found for key: " + std::string(key.begin(), key.end()));

    return std::get<ValPos>(*pos);
  }

  /// @brief Add an entry to the mapping
  /// @detail Overwrites existing entry
  /// @param key Key for the mapping entry
  /// @param value Value for the mapping entry
  template <class TKey, class TValue>
  void add(const TKey& key, const TValue& value)
  {
    std::lock_guard<std::mutex> klg(kv_mutex_);
    add_entry(
        kv_,
        key_t(reinterpret_cast<const std::uint8_t*>(key.data()), key.size()),
        value_t(reinterpret_cast<const std::uint8_t*>(value.data()), value.size()),
        CheckLen::Check);
  }

  /// @brief Copy-assignment operator
  void operator=(Mapping oth)
  {
    kv_ = std::forward<std::map<key_t, value_t>>(oth.kv_);
    buf_ = std::forward<buffer_t>(oth.buf_);
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a mutable reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another Mapping
  bool operator==(const Mapping& oth) const
  {
    return kv_ == oth.kv_;
  }

 private:
  void add_entry(std::map<key_t, value_t>& kv, const key_t& key, const value_t& value, const CheckLen& check)
  {
    const exception::Exception ex{"Router: Mapping", __func__};

    const auto& tot_len = entry_size(key, value) + size();
    const auto& max_len = tini2p::under_cast(MaxLen);
    if (tot_len > max_len)
      {
        ex.throw_ex<std::runtime_error>(
            "entry exceeds max mapping size, map size w/ entry: " + std::to_string(tot_len)
            + ", max map size: " + std::to_string(max_len));
      }

    if (check == CheckLen::Check)
      {
        check_param_len(key.size(), ex);
        check_param_len(value.size(), ex);
      }

    // update entry if present, otherwise store new entry
    kv[key] = value;
  }

  void check_param_len(const std::size_t& param_len, const exception::Exception& ex)
  {  // check parameter is in valid range
    const auto& min_kv_len = tini2p::under_cast(MinKVLen);
    const auto& max_kv_len = tini2p::under_cast(MaxKVLen);

    if (param_len < min_kv_len || param_len > max_kv_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid key-value size: " + std::to_string(param_len) + " min: " + std::to_string(min_kv_len)
            + " max: " + std::to_string(max_kv_len));
      }
  }

  std::map<key_t, value_t> kv_;
  std::mutex kv_mutex_;
  buffer_t buf_;
  const std::string kv_delim_{"="}, tail_delim_{";"};
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_ROUTER_MAPPING_H_
