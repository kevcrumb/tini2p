/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SRC_DATA_ROUTER_CERTIFICATE_H_
#define SRC_DATA_ROUTER_CERTIFICATE_H_

#include <iostream>
#include <type_traits>

#include "src/exception/exception.h"

#include "src/bytes.h"

#include "src/data/router/meta.h"

namespace tini2p
{
namespace data
{
/// @brief Container and processor of router certificates
class Certificate
{
 public:
  enum Sizes : std::uint8_t
  {
    CertTypeLen = 1,
    NullCertLen = 0,
    KeyCertLen = 4,
    LengthLen = 2,
    HeaderLen = CertTypeLen + LengthLen,
  };

  enum Offsets : std::uint8_t
  {
    CertTypeOffset = 0,
    LengthOffset,
    SignTypeOffset = 3,
    CryptoTypeOffset = 5,
  };

  enum Cert_t : std::uint8_t
  {
    NullCert = 0,
    HashCashCert,
    HiddenCert,
    SignedCert,
    MultipleCert,
    KeyCert
  };

  enum Signing_t : std::uint16_t
  {
    DSASha1 = 0,
    ECDSASha256P256,
    ECDSASha384P384,
    ECDSASha512P521,
    RSASha256_2048,
    RSASha384_3072,
    RSASha512_4096,
    EdDSASha512Ed25519,
    EdDSASha512Ed25519ph,
    Gost256,
    Gost512,
    RedDSA,
    XEdDSA,
    SigningUnsupported = 65534,
    SigningReserved = 65535,
    // convenience definition
    EdDSA = EdDSASha512Ed25519,
  };

  enum Crypto_t : std::uint16_t
  {
    ElGamal = 0,
    EciesP256,
    EciesP384,
    EciesP521,
    EciesX25519,
    EciesX25519Blake,
    CryptoUnsupported = 65534,
    CryptoReserved = 65535
  };

  using cert_type_t = Cert_t;
  using length_t = std::uint16_t;
  using sign_type_t = Signing_t;
  using crypto_type_t = Crypto_t;
  using buffer_t = crypto::SecBytes;

  cert_type_t cert_type;
  length_t length;
  sign_type_t sign_type;
  crypto_type_t crypto_type;
  buffer_t buffer;

  Certificate()
      : cert_type(cert_type_t::KeyCert),
        length(KeyCertLen),
        sign_type(sign_type_t::EdDSA),
        crypto_type(crypto_type_t::EciesX25519),
        locally_unreachable_(false),
        buffer(KeyCertLen)
  {
    serialize();
  }

  /// @brief Create a Certificate by certificate type
  /// @detail Valid types are NullCert and KeyCert.
  ///     KeyCert defaults to EdDSA signing type, and ECIES-X25519-AEAD-Ratchet crypto type
  /// @throw Invalid argument for unsupported certificate types
  explicit Certificate(cert_type_t type) : cert_type(std::forward<cert_type_t>(type)), buffer()
  {
    const exception::Exception ex{"Router: Certificate", __func__};

    check_cert_type(ex);

    if (cert_type == cert_type_t::KeyCert)
      {
        length = KeyCertLen;
        sign_type = sign_type_t::EdDSA;
        crypto_type = crypto_type_t::EciesX25519;
      }
    else
      {
        length = NullCertLen;
        sign_type = sign_type_t::SigningReserved;
        crypto_type = crypto_type_t::CryptoReserved;
      }

    locally_unreachable_ = false;
    
    serialize();
  }

  /// @brief Create a certificate with given sign + crypto types
  Certificate(
      const sign_type_t sign_type_in,
      const crypto_type_t crypto_type_in)
      : cert_type(cert_type_t::KeyCert),
        length(KeyCertLen),
        sign_type(sign_type_in),
        crypto_type(crypto_type_in),
        locally_unreachable_(false),
        buffer(KeyCertLen)
  {
    serialize();
  }

  /// @brief Create a certificate with given sign + crypto type info
  Certificate(
      const std::type_info& sign_type_info,
      const std::type_info& crypto_type_info)
      : cert_type(cert_type_t::KeyCert),
        length(KeyCertLen),
        locally_unreachable_(false),
        buffer(KeyCertLen)
  {
    type_info_to_type(sign_type_info, crypto_type_info);

    serialize();
  }

  /// @brief Convert signing + crypto type_info to sign + crypto types
  void type_info_to_type(
      const std::type_info& sign_type_info,
      const std::type_info& crypto_type_info)
  {
    using eddsa_t = tini2p::crypto::EdDSASha512;
    using reddsa_t = tini2p::crypto::RedDSASha512;
    using xeddsa_t = tini2p::crypto::XEdDSASha512;

    using ecies_x25519_hmac_t = tini2p::crypto::EciesX25519<tini2p::crypto::HmacSha256>;
    using ecies_x25519_blake_t = tini2p::crypto::EciesX25519<tini2p::crypto::Blake2b>;

    sign_type = sign_type_info == typeid(eddsa_t)
                    ? sign_type_t::EdDSA
                    : sign_type_info == typeid(reddsa_t)
                          ? sign_type_t::RedDSA
                          : sign_type_info == typeid(xeddsa_t)
                                ? sign_type_t::XEdDSA
                                : sign_type_t::SigningUnsupported;

    crypto_type = crypto_type_info == typeid(ecies_x25519_hmac_t)
                      ? crypto_type_t::EciesX25519
                      : crypto_type_info == typeid(ecies_x25519_blake_t)
                            ? crypto_type_t::EciesX25519Blake
                            : crypto_type_t::CryptoUnsupported;
  }

  /// @brief Serialize the certificate to buffer
  void serialize()
  {
    const exception::Exception ex{"Router: Certificate", __func__};

    buffer.resize(size());

    check_params(ex);

    tini2p::BytesWriter<buffer_t> writer(buffer);

    writer.write_bytes(cert_type);
    writer.write_bytes(length, tini2p::Endian::Big);

    if (cert_type == cert_type_t::KeyCert)
      {
        writer.write_bytes(sign_type, tini2p::Endian::Big);
        writer.write_bytes(crypto_type, tini2p::Endian::Big);
      }
  }

  /// @brief Deserialize the certificate from buffer
  void deserialize()
  {
    const exception::Exception ex{"Router: Certificate", __func__};

    tini2p::BytesReader<buffer_t> reader(buffer);

    reader.read_bytes(cert_type);
    reader.read_bytes(length, tini2p::Endian::Big);

    check_length(ex);

    if (cert_type == cert_type_t::KeyCert)
      {
        reader.read_bytes(sign_type, tini2p::Endian::Big);
        reader.read_bytes(crypto_type, tini2p::Endian::Big);
      }
    else if (cert_type == cert_type_t::NullCert)
      {
        sign_type = sign_type_t::SigningReserved;
        crypto_type = crypto_type_t::CryptoReserved;
      }

    check_params(ex);

    // reclaim unused space
    buffer.resize(reader.count());
    buffer.shrink_to_fit();
  }

  std::uint16_t size() const noexcept
  {
    return HeaderLen + length;
  }

  /// @brief Get unreachable status
  /// @detail Identity/Destination is unreachable if cert has unsupported crypto
  constexpr bool locally_unreachable() const noexcept
  {
    return locally_unreachable_;
  }

  /// @brief Compare equality with another Certificate
  bool operator==(const Certificate& oth) const
  {  // attempt constant-time comparison
    const auto& type_eq = static_cast<std::uint8_t>(cert_type == oth.cert_type);
    const auto& len_eq = static_cast<std::uint8_t>(length == oth.length);
    const auto& sign_eq = static_cast<std::uint8_t>(sign_type == oth.sign_type);
    const auto& crypto_eq = static_cast<std::uint8_t>(crypto_type == oth.crypto_type);

    return static_cast<bool>(type_eq * len_eq * sign_eq * crypto_eq);
  }

 private:
  void check_params(const tini2p::exception::Exception& ex)
  {
    check_cert_type(ex);
    check_length(ex);

    if (cert_type == cert_type_t::KeyCert)
    {
      switch (crypto_type)
        {
          case crypto_type_t::EciesX25519:
          case crypto_type_t::EciesX25519Blake:
            break;
          default:
            locally_unreachable_ = true;
            break;
        };

      switch (sign_type)
        {
          case sign_type_t::EdDSA:
          case sign_type_t::RedDSA:
          case sign_type_t::XEdDSA:
            break;
          default:
            locally_unreachable_ = true;
            break;
        };
    }
  }

  void check_length(const exception::Exception& ex) const
  {
    const auto& expected_len =
        cert_type == cert_type_t::KeyCert ? tini2p::under_cast(KeyCertLen) : tini2p::under_cast(NullCertLen);
    if (length != expected_len)
      {
        ex.throw_ex<std::runtime_error>(
            "invalid certificate length: " + std::to_string(length) + " expected: " + std::to_string(expected_len));
      }
  }

  void check_cert_type(const exception::Exception& ex) const
  {
    if (cert_type != cert_type_t::NullCert && cert_type != cert_type_t::KeyCert)
      ex.throw_ex<std::runtime_error>("invalid certificate type.");
  }

  bool locally_unreachable_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_ROUTER_CERTIFICATE_H_
