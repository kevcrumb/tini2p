/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SRC_NETDB_KADEMLIA_H_
#define SRC_NETDB_KADEMLIA_H_

#include <date/date.h>

#include "src/bytes.h"
#include "src/time.h"
#include "src/exception/exception.h"

#include "src/crypto/keys.h"
#include "src/crypto/sec_bytes.h"
#include "src/crypto/sha.h"

namespace tini2p
{
namespace netdb
{
struct Kademlia
{
  enum
  {
    DateLen = 8,
    RoutingKeyLen = crypto::Sha256::DigestLen,
    MetricLen = crypto::Sha256::DigestLen,
  };

  struct RoutingKey : public crypto::Key<RoutingKeyLen>
  {
    using base_t = crypto::Key<RoutingKeyLen>;

    RoutingKey() : base_t() {}

    RoutingKey(base_t::buffer_t buf) : base_t(std::forward<base_t::buffer_t>(buf)) {}

    RoutingKey(const crypto::SecBytes& buf) : base_t(buf) {}

    RoutingKey(std::initializer_list<std::uint8_t> list) : base_t(list) {}
  };

  using date_t = crypto::FixedSecBytes<DateLen>;  //< Date trait alias
  using routing_key_t = RoutingKey;  //< Routing key trait alias
  using search_key_t = crypto::Sha256::digest_t;  //< Search key trait alias
  using metric_t = crypto::Key<MetricLen>;  //< Metric trait alias

  static void CreateRoutingKey(const crypto::Sha256::digest_t& search_key, routing_key_t& routing_key)
  {
    crypto::FixedSecBytes<RoutingKeyLen + DateLen> search_key_date;

    // get current date as ASCII date-string: yyyyMMdd
#if defined(TINI2P_TESTS)
    date_t date(reinterpret_cast<const std::uint8_t*>("20030401"), DateLen);
#else
    date_t date(reinterpret_cast<const std::uint8_t*>(tini2p::time::now_date().data()), DateLen);
#endif

    // write input for left routing key: SearchKey || Date
    tini2p::BytesWriter<decltype(search_key_date)> writer(search_key_date);
    writer.write_data(search_key);
    writer.write_data(date);

    // calculate routing key: H(SearchKey || Date)
    crypto::Sha256::Hash(routing_key.buffer(), search_key_date);
  }

  /// @brief Calculate Kademlia XOR distance metric
  /// @param routing_key Routing key of the entry being stored/looked up 
  /// @param entry Search key (Identity/Destination hash) to find closest router in the DHT neighborhood
  static void XOR(const routing_key_t& routing_key, const search_key_t& entry, metric_t& metric)
  {  // calculate routing key ^ entry search key
    metric = routing_key.buffer() ^ entry;
  }
};
}  // namespace netdb
}  //namespace tini2p

#endif  // SRC_NETDB_KADEMLIA_H_
