/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SRC_NETDB_NETDB_H_
#define SRC_NETDB_NETDB_H_

#include <deque>
#include <mutex>
#include <set>

#include "src/data/router/info.h"
#include "src/data/router/lease_set.h"

#include "src/data/i2np/database_store.h"
#include "src/data/i2np/database_lookup.h"
#include "src/data/i2np/database_search_reply.h"
#include "src/data/i2np/delivery_status.h"

#include "src/netdb/kademlia.h"

namespace tini2p
{
namespace netdb
{
/// @class NetDB
class NetDB
{
 public:
  enum
  {
    DefaultPeersLen = 3,  //< default closest peers length, see spec
    MaxRouterInfos = 666,  //< arbitrary max, adjust for performance/security
    MaxLeaseSets = 1337,  //< arbitrary max, adjust for performance/security
    MaxUnsupportedRI = 25,
    MaxUnsupportedLS = 25,
  };

  using lookup_reply_t =
      std::variant<data::DatabaseStore, data::DatabaseSearchReply>;  //< DatabaseLookup reply trait alias
  using router_infos_t = std::deque<data::Info::shared_ptr>;  //< RouterInfo storage trait alias
  using floodfills_t = std::vector<data::Info::shared_ptr>;  //< Floodfill storage trait alias
  using lease_sets_t = std::deque<data::LeaseSet2::shared_ptr>;  //< LeaseSet2 storage trait alias
  using peers_t = std::vector<crypto::Sha256::digest_t>;  //< Peers trait alias

  /// @brief Create a NetDB
  /// @param own_info Shared pointer to our RouterInfo
  NetDB(data::Info::shared_ptr own_info) : own_info_(own_info), infos_(), lease_sets_() {}

  /// @brief Handle storing entries from a DatabaseStore message
  /// @detail Stores only supported entries, and relays verifiable unsupported entries
  /// @note Caller is responsible for sending appropriate reply message
  /// @param msg DatabaseStore message to handle
  /// @throw Logic error for type mismatch, and invalid entry types
  void HandleDatabaseStore(const data::DatabaseStore& msg)
  {
    const exception::Exception ex{"NetDB", __func__};

    const auto type = msg.type();
    const auto entry = msg.entry_data();
    // TODO(tini2p): these nested conditionals are gross, refactor
    if (type == data::DatabaseStore::type_t::RouterInfo)
      {
        if (std::holds_alternative<data::Info::shared_ptr>(entry))
          {
            if (msg.locally_reachable())
              {
                if (infos_.size() < MaxRouterInfos && msg.Verify())
                  {
                    auto info_ptr = std::get<data::Info::shared_ptr>(entry);
                    if (info_ptr->is_floodfill())
                      {  // lock floodfills
                        std::lock_guard<std::mutex> fgd(floodfill_mutex_);
                        floodfills_.push_back(info_ptr);
                      }  // end-floodfills-lock
                    else
                      {  // lock infos
                        std::lock_guard<std::mutex> igd(info_mutex_);
                        infos_.push_back(info_ptr);
                      }  // end-infos-lock
                  }
              }
            else
              {
                if (unsupported_infos_.size() < MaxUnsupportedRI && msg.Verify())
                  {  // lock unsupported infos
                    std::lock_guard<std::mutex> uigd(uninfo_mutex_);
                    unsupported_infos_.push_back(std::get<data::Info::shared_ptr>(entry));
                  }  // end-unsupported-infos-lock
              }
          }
        else
          ex.throw_ex<std::logic_error>("invalid data type.");
      }
    else if (type == data::DatabaseStore::type_t::LeaseSet2)
      {
        if (std::holds_alternative<data::LeaseSet2::shared_ptr>(entry))
          {
            if (msg.locally_reachable())
              {
                if (lease_sets_.size() < MaxLeaseSets && msg.Verify())
                {  // lease_sets lock
                  std::lock_guard<std::mutex> lgd(ls_mutex_);
                  lease_sets_.push_back(std::get<data::LeaseSet2::shared_ptr>(entry));
                }  // end-lease_sets-lock
              }
            else
              {
                if (unsupported_lease_sets_.size() < MaxUnsupportedLS && msg.Verify())
                  {  // unsupported lease_sets lock
                    std::lock_guard<std::mutex> ulgd(unls_mutex_);
                    unsupported_lease_sets_.push_back(std::get<data::LeaseSet2::shared_ptr>(entry));
                  }  // end-unsupported-lease_sets-lock
              }
          }
        else
          ex.throw_ex<std::logic_error>("invalid data type.");
      }
    else
      ex.throw_ex<std::logic_error>("NetDB: unsupported entry type: " + std::to_string(tini2p::under_cast(type)));
  }

  /// @brief Handle received DatabaseLookup message
  /// @detail Searches local RouterInfo & LeaseSet storage for search match.
  ///     Does not consider unsupported entries for matches.
  /// @note Essentialy a const function, but needs non-const to lock mutexes for thread safety
  /// @note Caller is responsible for sending appropriate reply message
  /// @param msg DatabaseLookup message to handle
  /// @param reply Reply variant that will contain a DatabaseStore message if entry found, or DatabaseSearchReply if not
  void HandleDatabaseLookup(const data::DatabaseLookup& msg, lookup_reply_t& reply)
  {
    const auto& key = msg.search_key();
    const auto& lookup = msg.lookup_flags();
    bool unfound = false;

    {  // ri ff ls lock
      std::lock_guard<std::mutex> rgd(info_mutex_);
      std::lock_guard<std::mutex> fgd(floodfill_mutex_);
      std::lock_guard<std::mutex> lgd(ls_mutex_);

      const auto ri_iter =
          std::find_if(infos_.begin(), infos_.end(), [&key](const auto& e) { return e->hash() == key; });
      const auto ri_found = ri_iter != infos_.end();

      const auto ff_iter =
          std::find_if(floodfills_.begin(), floodfills_.end(), [&key](const auto& e) { return e->hash() == key; });
      const auto ff_found = ff_iter != floodfills_.end();

      const auto ls_iter =
          std::find_if(lease_sets_.begin(), lease_sets_.end(), [&key](const auto& e) { return e->hash() == key; });
      const auto ls_found = ls_iter != lease_sets_.end();

      if (lookup == data::DatabaseLookup::LookupFlags::Normal)
        {
          if (ri_found)
            reply.emplace<data::DatabaseStore>(*ri_iter);
          else if (ff_found)
            reply.emplace<data::DatabaseStore>(*ff_iter);
          else if (ls_found)
            reply.emplace<data::DatabaseStore>(*ls_iter);
          else
            unfound = true;
        }
      else if (lookup == data::DatabaseLookup::LookupFlags::LS)
        {
          if (ls_found)
            reply.emplace<data::DatabaseStore>(*ls_iter);
          else
            unfound = true;
        }
      else if (lookup == data::DatabaseLookup::LookupFlags::RI)
        {
          if (ri_found)
            reply.emplace<data::DatabaseStore>(*ri_iter);
          else if (ff_found)
            reply.emplace<data::DatabaseStore>(*ff_iter);
          else
            unfound = true;
        }
    }  // end-ri-ff-ls-lock

    if (lookup == data::DatabaseLookup::LookupFlags::Explore || unfound)
      reply.emplace<data::DatabaseSearchReply>(key, own_info_->hash(), ClosestPeers(key, lookup));
  }

  /// @brief Get a const reference to the non-floodfill RouterInfos
  const router_infos_t& infos() const noexcept
  {
    return infos_;
  }

  /// @brief Get a const reference to the floodfill RouterInfos
  const floodfills_t& floodfills() const noexcept
  {
    return floodfills_;
  }

  /// @brief Get a const reference to the LeaseSets
  const lease_sets_t& lease_sets() const noexcept
  {
    return lease_sets_;
  }

  /// @brief Get the peers closest to the search key
  /// @note Essentially a const function, but needs non-const to lock mutexes for thread safety
  /// @param key Search key (Sha256 of RouterIdentity/Destination, not RoutingKey)
  /// @param flag Lookup flag for the search (Explore lookup only returns non-floodfill peers)
  peers_t ClosestPeers(const data::DatabaseLookup::search_key_t& key, const data::DatabaseLookup::LookupFlags& flag)
  {
    using close_peers_t = std::map<Kademlia::metric_t, Kademlia::search_key_t>;

    Kademlia::routing_key_t routing_key;
    Kademlia::CreateRoutingKey(key, routing_key);

    close_peers_t nonff_peers;
    close_peers_t ff_peers;

    {  // ri ff lock
      std::lock_guard<std::mutex> rgd(info_mutex_);
      std::lock_guard<std::mutex> fgd(floodfill_mutex_);

      /// order the closest non-floodfill peers
      for (const auto& ri : infos_)
        {
          const auto& key = ri->hash();
          Kademlia::metric_t metric;
          Kademlia::XOR(routing_key, key, metric);
          nonff_peers.try_emplace(std::move(metric), key);
        }

      /// order the closest floodfill peers
      for (const auto& ff : floodfills_)
        {
          const auto& key = ff->hash();
          Kademlia::metric_t metric;
          Kademlia::XOR(routing_key, key, metric);
          ff_peers.emplace(std::move(metric), key);
        }
    }  // end-ri-ff-lock

    // if this is an Explore lookup, only consider non-floodfill peers.
    // otherwise, merge all peers into the closest peers list
    if (flag != data::DatabaseLookup::LookupFlags::Explore)
      nonff_peers.merge(ff_peers);  // add floodfill peers to non-floodfill peers

    // get the number of peers to return
    // temporary assignment necessary for memory safety of std::min return type (returns `const T&`)
    const auto& default_peers = static_cast<std::size_t>(DefaultPeersLen);
    const auto& available_peers = nonff_peers.size();
    const auto& num_peers = std::min(default_peers, available_peers);

    peers_t peers;
    peers.reserve(num_peers);
    std::uint8_t n = 0;

    // add the closest `n` peers to return list
    for (auto& peer : nonff_peers)
      {
        if (n == num_peers)
          break;

        peers.emplace_back(std::move(std::get<Kademlia::search_key_t>(peer)));
        ++n;
      }

    return peers;
  }

 private:
  data::Info::shared_ptr own_info_;
  router_infos_t infos_;
  std::mutex info_mutex_;
  floodfills_t floodfills_;
  std::mutex floodfill_mutex_;
  std::vector<data::Info::shared_ptr> unsupported_infos_;
  std::mutex uninfo_mutex_;
  lease_sets_t lease_sets_;
  std::mutex ls_mutex_;
  std::vector<data::LeaseSet2::shared_ptr> unsupported_lease_sets_;
  std::mutex unls_mutex_;
};
}  // namespace netdb
}  // namespace tini2p

#endif  // SRC_NETDB_NETDB_H_
