/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <catch2/catch.hpp>

#include "src/gzip.h"

#include <iostream>
using Catch::Matchers::Equals;
using vec = std::vector<std::uint8_t>;

TEST_CASE("Gzip compresses a buffer", "[gzip]")
{
  vec in(tini2p::Gzip::MaxLen);
  vec out;

  REQUIRE_NOTHROW(tini2p::Gzip::Compress(in.data(), tini2p::Gzip::MaxLen, out));
  REQUIRE(out.size() < tini2p::Gzip::MaxLen);
  REQUIRE(out.size() > tini2p::Gzip::MinLen);
}

TEST_CASE("Gzip decompresses a buffer", "[gzip]")
{
  vec in{{0x78, 0x5e, 0x63, 0x60, 0xa0, 0x0c, 0x00, 0x00, 0x00, 0x40, 0x00, 0x01}};
  vec out;
  vec exp_out(64);  // 64 zeros

  REQUIRE_NOTHROW(tini2p::Gzip::Decompress(in.data(), tini2p::Gzip::MaxLen, out));
  REQUIRE(out.size() <= tini2p::Gzip::MaxLen);
  REQUIRE(out.size() > tini2p::Gzip::MinLen);
  REQUIRE_THAT(out, Equals(exp_out));
}

TEST_CASE("Gzip rejects null buffers", "[gzip]")
{
  vec out{};

  REQUIRE_THROWS(tini2p::Gzip::Compress(nullptr, tini2p::Gzip::MinLen, out));
  REQUIRE_THROWS(tini2p::Gzip::Compress(out.data(), 0, out));

  REQUIRE_THROWS(tini2p::Gzip::Decompress(nullptr, tini2p::Gzip::MinLen, out));
  REQUIRE_THROWS(tini2p::Gzip::Decompress(out.data(), 0, out));
}

TEST_CASE("Gzip rejects oversize buffers", "[gzip]")
{
  vec out{};

  REQUIRE_THROWS(tini2p::Gzip::Compress(out.data(), tini2p::Gzip::MaxLen + 1, out));
  REQUIRE_THROWS(tini2p::Gzip::Decompress(out.data(), tini2p::Gzip::MaxLen + 1, out));
}
