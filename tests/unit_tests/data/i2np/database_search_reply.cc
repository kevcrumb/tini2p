/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/data/i2np/database_search_reply.h"

using tini2p::data::DatabaseSearchReply;

struct DatabaseSearchReplyFixture
{
  DatabaseSearchReplyFixture()
  {
    // mock random keys, unrealistic
    tini2p::crypto::RandBytes(search_key);
    tini2p::crypto::RandBytes(peer_key);
    tini2p::crypto::RandBytes(from_key);
  }

  DatabaseSearchReply::search_key_t search_key;
  DatabaseSearchReply::peer_key_t peer_key;
  DatabaseSearchReply::from_key_t from_key;

  std::unique_ptr<DatabaseSearchReply> dbsr_ptr;
};

TEST_CASE_METHOD(DatabaseSearchReplyFixture, "DatabaseSearchReply has valid fields", "[i2np]")
{
  REQUIRE_NOTHROW(dbsr_ptr.reset(new DatabaseSearchReply(search_key, from_key)));
  REQUIRE(dbsr_ptr->search_key() == search_key);
  REQUIRE(dbsr_ptr->peer_keys().empty());
  REQUIRE(dbsr_ptr->from_key() == from_key);

  REQUIRE_NOTHROW(dbsr_ptr.reset(new DatabaseSearchReply(search_key, from_key, {peer_key})));
  REQUIRE(dbsr_ptr->search_key() == search_key);
  REQUIRE(dbsr_ptr->peer_keys().size() == 1);
  REQUIRE(dbsr_ptr->from_key() == from_key);
}

TEST_CASE_METHOD(DatabaseSearchReplyFixture, "DatabaseSearchReply deserializes from buffer", "[i2np]")
{
  std::unique_ptr<DatabaseSearchReply> dbsr_des_ptr;
  REQUIRE_NOTHROW(dbsr_ptr.reset(new DatabaseSearchReply(search_key, from_key)));
  REQUIRE_NOTHROW(dbsr_des_ptr.reset(new DatabaseSearchReply(dbsr_ptr->buffer())));

  REQUIRE(*dbsr_ptr == *dbsr_des_ptr);
}

TEST_CASE_METHOD(DatabaseSearchReplyFixture, "DatabaseSearchReply rejects null keys", "[i2np]")
{
  REQUIRE_THROWS(DatabaseSearchReply({}, from_key));
  REQUIRE_THROWS(DatabaseSearchReply(search_key, {}));

  peer_key.zero();  // zero the peer key, closest router hash will never be null
  REQUIRE_THROWS(DatabaseSearchReply(search_key, from_key, {peer_key}));
  REQUIRE_THROWS(DatabaseSearchReply(search_key, from_key).peer_keys({peer_key}));
}

TEST_CASE_METHOD(DatabaseSearchReplyFixture, "DatabaseSearchReply rejects excessive peer keys", "[i2np]")
{
  std::vector<DatabaseSearchReply::peer_key_t> peer_keys(DatabaseSearchReply::MaxPeers + 1);
  for (auto& peer : peer_keys)
    tini2p::crypto::RandBytes(peer);

  REQUIRE_THROWS(DatabaseSearchReply(search_key, from_key, peer_keys));
  REQUIRE_THROWS(DatabaseSearchReply(search_key, from_key).peer_keys(peer_keys));
}
