/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/data/i2np/delivery_status.h"

using tini2p::data::DeliveryStatus;

struct DeliveryStatusFixture
{
  DeliveryStatusFixture() : msg_id(tini2p::crypto::RandInRange()), ds_ptr(new DeliveryStatus(msg_id)) {}

  DeliveryStatus::message_id_t msg_id;
  DeliveryStatus::timestamp_t time;
  std::unique_ptr<DeliveryStatus> ds_ptr;
};

TEST_CASE_METHOD(DeliveryStatusFixture, "DeliveryStatus has valid fields", "[i2np]")
{
  REQUIRE(ds_ptr->message_id() == msg_id);
  REQUIRE(ds_ptr->timestamp() == tini2p::time::now_s());

  time = tini2p::time::now_s() - 500;  // set timestamp to arbitrary point in the past
  REQUIRE_NOTHROW(ds_ptr.reset(new DeliveryStatus(msg_id, time)));

  REQUIRE(ds_ptr->message_id() == msg_id);
  REQUIRE(ds_ptr->timestamp() == time);
}

TEST_CASE_METHOD(DeliveryStatusFixture, "DeliveryStatus deserializes from buffer", "[i2np]")
{
  std::unique_ptr<DeliveryStatus> ds_des_ptr;
  REQUIRE_NOTHROW(ds_des_ptr.reset(new DeliveryStatus(ds_ptr->buffer())));
  REQUIRE(*ds_des_ptr == *ds_ptr);

  REQUIRE_NOTHROW(ds_des_ptr.reset(new DeliveryStatus(ds_ptr->buffer().data(), DeliveryStatus::MessageLen)));
  REQUIRE(*ds_des_ptr == *ds_ptr);
}

TEST_CASE_METHOD(DeliveryStatusFixture, "DeliveryStatus rejects invalid buffer", "[i2np]")
{
  REQUIRE_THROWS(DeliveryStatus(nullptr, DeliveryStatus::MessageLen));
  REQUIRE_THROWS(DeliveryStatus(ds_ptr->buffer().data(), DeliveryStatus::MessageLen - 1));
  REQUIRE_THROWS(DeliveryStatus(ds_ptr->buffer().data(), DeliveryStatus::MessageLen + 1));
}
