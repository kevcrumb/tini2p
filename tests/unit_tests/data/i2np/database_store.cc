/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <catch2/catch.hpp>

#include "src/data/i2np/database_store.h"

TEST_CASE("DatabaseStore message has valid fields", "[i2np]")
{
  /*----------------------------------------------------------------------\
  | RouterInfo DatabaseStore message (no reply)                           |
  \----------------------------------------------------------------------*/
  tini2p::data::Info::shared_ptr info(new tini2p::data::Info());
  tini2p::data::DatabaseStore dbs_ri(info);

  REQUIRE(dbs_ri.search_key() == info->hash());
  REQUIRE(dbs_ri.type() == tini2p::data::DatabaseStore::Type::RouterInfo);
  REQUIRE(dbs_ri.reply_token() == 0);
  REQUIRE(dbs_ri.reply_tunnel_id() == 0);
  REQUIRE(dbs_ri.reply_gateway() == tini2p::crypto::Sha256::digest_t());
  REQUIRE(*(std::get<tini2p::data::Info::shared_ptr>(dbs_ri.entry_data())) == *info);

  /*----------------------------------------------------------------------\
  | LeaseSet2 DatabaseStore message (no reply)                            |
  \----------------------------------------------------------------------*/
  tini2p::data::LeaseSet2::shared_ptr ls(new tini2p::data::LeaseSet2());
  tini2p::data::DatabaseStore dbs_ls(ls);

  REQUIRE(dbs_ls.search_key() == ls->hash());
  REQUIRE(dbs_ls.type() == tini2p::data::DatabaseStore::Type::LeaseSet2);
  REQUIRE(dbs_ls.reply_token() == 0);
  REQUIRE(dbs_ls.reply_tunnel_id() == 0);
  REQUIRE(dbs_ls.reply_gateway() == tini2p::crypto::Sha256::digest_t());
  REQUIRE(*(std::get<tini2p::data::LeaseSet2::shared_ptr>(dbs_ls.entry_data())) == *ls);

  /*----------------------------------------------------------------------\
  | Reply DatabaseStore message                                           |
  \----------------------------------------------------------------------*/
  tini2p::data::DatabaseStore::reply_tunnel_id_t tunnel_id(0x42);
  tini2p::data::Info reply_ri;
  tini2p::data::DatabaseStore::reply_gateway_t gateway(reply_ri.hash());
  REQUIRE_NOTHROW(dbs_ri.reply(tunnel_id, gateway));

  REQUIRE(dbs_ri.search_key() == info->hash());
  REQUIRE(dbs_ri.type() == tini2p::data::DatabaseStore::Type::RouterInfo);
  REQUIRE(dbs_ri.reply_token() != 0);
  REQUIRE(dbs_ri.reply_tunnel_id() == tunnel_id);
  REQUIRE(dbs_ri.reply_gateway() == gateway);
  REQUIRE(*(std::get<tini2p::data::Info::shared_ptr>(dbs_ri.entry_data())) == *info);
}

TEST_CASE("DatabaseStore deserializes from a buffer", "[i2np]")
{
  /*----------------------------------------------------------------------\
  | RouterInfo DatabaseStore message deserialization                      |
  \----------------------------------------------------------------------*/
  tini2p::data::Info::shared_ptr info(new tini2p::data::Info());
  auto dbs = std::make_unique<tini2p::data::DatabaseStore>(info);

  const auto& dbs_buf = dbs->buffer();

  REQUIRE_NOTHROW(tini2p::data::DatabaseStore(dbs_buf));
  REQUIRE_NOTHROW(tini2p::data::DatabaseStore(dbs_buf.data(), dbs_buf.size()));

  /*----------------------------------------------------------------------\
  | LeaseSet2 DatabaseStore message deserialization                       |
  \----------------------------------------------------------------------*/
  tini2p::data::LeaseSet2::shared_ptr ls(new tini2p::data::LeaseSet2());
  dbs.reset(new tini2p::data::DatabaseStore(ls));

  REQUIRE_NOTHROW(tini2p::data::DatabaseStore(dbs->buffer()));
}

TEST_CASE("DatabaseStore rejects invalid buffer", "[i2np]")
{
  tini2p::data::DatabaseStore::buffer_t buf;
  REQUIRE_THROWS(tini2p::data::DatabaseStore(nullptr, tini2p::data::DatabaseStore::MinLen));
  REQUIRE_THROWS(tini2p::data::DatabaseStore(buf.data(), 0));

  buf.resize(tini2p::data::DatabaseStore::MaxLen + 1);
  REQUIRE_THROWS(tini2p::data::DatabaseStore(buf.data(), buf.size()));
}
