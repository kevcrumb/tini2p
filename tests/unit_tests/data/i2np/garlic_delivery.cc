/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/data/i2np/garlic_delivery.h"

using GDI = tini2p::data::GarlicDeliveryInstructions;

struct GDIFixture
{
  GDIFixture()
  {
    tini2p::crypto::RandBytes(to_hash);  // simulate "to hash", unrealistic
  }

  GDI gd;
  GDI::hash_t to_hash;
};

TEST_CASE_METHOD(GDIFixture, "GarlicDeliveryInstructions have valid default fields", "[i2np]")
{
  REQUIRE(gd.flags() == tini2p::under_cast(GDI::DeliveryFlags::Local));
  REQUIRE(gd.to_hash().is_zero());
  REQUIRE(gd.tunnel_id() == 0);
  REQUIRE(gd.delay() == 0);
  REQUIRE(gd.size() == GDI::FlagsLen);
}

TEST_CASE_METHOD(GDIFixture, "GarlicDeliveryInstructions creates fully initialized instructions", "[i2np]")
{
  REQUIRE_NOTHROW(GDI(GDI::DeliveryFlags::Destination, to_hash));
  REQUIRE_NOTHROW(GDI(GDI::DeliveryFlags::Router, to_hash));
  REQUIRE_NOTHROW(GDI(GDI::DeliveryFlags::Tunnel, to_hash, GDI::tunnel_id_t(0x42)));
}

TEST_CASE_METHOD(GDIFixture, "GarlicDeliveryInstructions deserializes from a buffer", "[i2np]")
{
  REQUIRE_NOTHROW(gd = std::move(GDI(GDI::DeliveryFlags::Tunnel, to_hash, GDI::tunnel_id_t(0x42))));
  std::unique_ptr<GDI> gd_ptr;

  REQUIRE_NOTHROW(gd_ptr.reset(new GDI(gd.buffer().data(), gd.size())));
  REQUIRE(*gd_ptr == gd);

  REQUIRE_NOTHROW(gd_ptr.reset(new GDI(gd.buffer())));
  REQUIRE(*gd_ptr == gd);

  REQUIRE_NOTHROW(gd_ptr->from_buffer(gd.buffer()));
  REQUIRE(*gd_ptr == gd);
}

TEST_CASE_METHOD(GDIFixture, "GarlicDeliveryInstructions rejects invalid full init ctor", "[i2np]")
{
  REQUIRE_THROWS(GDI(GDI::DeliveryFlags::Destination, {}));  // rejects null "to hash"
  REQUIRE_THROWS(GDI(GDI::DeliveryFlags::Local, to_hash));  // rejects Local delivery w/ a "to hash"
  REQUIRE_THROWS(GDI(GDI::DeliveryFlags::Tunnel, to_hash, 0));  // rejects null tunnel ID w/ Tunnel delivery
}

TEST_CASE_METHOD(GDIFixture, "GarlicDeliveryInstructions rejects invalid deserialize ctor", "[i2np]")
{
  REQUIRE_THROWS(GDI(nullptr, GDI::MinLen));
  REQUIRE_THROWS(GDI(gd.buffer().data(), 0));
  REQUIRE_THROWS(GDI(gd.buffer().data(), GDI::MinLen - 1));
  REQUIRE_THROWS(GDI(gd.buffer().data(), GDI::MaxLen + 1));
}
