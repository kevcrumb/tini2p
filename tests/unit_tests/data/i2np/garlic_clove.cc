/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/data/i2np/garlic_clove.h"
#include "src/data/i2np/i2np.h"

using GarlicClove = tini2p::data::GarlicClove;
using GDI = tini2p::data::GarlicDeliveryInstructions;

// supported I2NP messages
using Data = tini2p::data::Data;
using DatabaseStore = tini2p::data::DatabaseStore;
using DeliveryStatus = tini2p::data::DeliveryStatus;

struct GarlicCloveFixture
{
  GDI instructions;
  std::unique_ptr<GarlicClove> clove_ptr;
};

TEST_CASE_METHOD(GarlicCloveFixture, "GarlicClove created from valid instruction + I2NP message", "[i2np]")
{
  const auto check_fields = [this](const auto& msg) {
    using msg_type_t = std::decay_t<decltype(msg)>;

    REQUIRE(clove_ptr->instructions() == instructions);
    REQUIRE(std::get<msg_type_t>(clove_ptr->message()) == msg);
    REQUIRE(clove_ptr->id() != GarlicClove::NullID);
    REQUIRE(clove_ptr->expiration() == tini2p::time::now_s() + GarlicClove::Expiration);
    REQUIRE(clove_ptr->cert() == tini2p::data::Certificate(tini2p::data::Certificate::cert_type_t::NullCert));
  };

  auto data_ptr = std::make_shared<Data>();
  REQUIRE_NOTHROW(clove_ptr.reset(new GarlicClove(instructions, data_ptr)));
  check_fields(data_ptr);

  auto dbs_ptr = std::make_shared<DatabaseStore>(std::make_shared<tini2p::data::Info>());
  REQUIRE_NOTHROW(clove_ptr.reset(new GarlicClove(instructions, dbs_ptr)));
  check_fields(dbs_ptr);

  auto dlv_ptr = std::make_shared<DeliveryStatus>(0x42);
  REQUIRE_NOTHROW(clove_ptr.reset(new GarlicClove(instructions, dlv_ptr)));
  check_fields(dlv_ptr);
}

TEST_CASE_METHOD(GarlicCloveFixture, "GarlicClove created from deserializing a valid buffer", "[i2np]")
{
  auto dbs_ptr = std::make_shared<DatabaseStore>(std::make_shared<tini2p::data::LeaseSet2>());
  REQUIRE_NOTHROW(clove_ptr.reset(new GarlicClove(instructions, dbs_ptr)));

  std::unique_ptr<GarlicClove> n_clove_ptr;
  REQUIRE_NOTHROW(n_clove_ptr.reset(new GarlicClove(clove_ptr->buffer())));
  REQUIRE(*n_clove_ptr == *clove_ptr);
}

TEST_CASE_METHOD(GarlicCloveFixture, "GarlicClove rejects creation from invalid I2NP message", "[i2np]")
{
  // no need to test invalid serializing ctor, will fail to compile if passed invalid variant alternative
  REQUIRE_NOTHROW(clove_ptr.reset(new GarlicClove(instructions, std::make_shared<Data>())));

  /*-----------------------------------------------\
   | Reset I2NP message type to unsupported types  |
   *----------------------------------------------*/
  const auto i2np_type_offset = instructions.size() + tini2p::data::I2NPBlock::I2NPTypeOffset;

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPBlock::MessageType::DatabaseSearchReply);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPBlock::MessageType::DatabaseLookup);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPBlock::MessageType::Garlic);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPBlock::MessageType::TunnelData);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPBlock::MessageType::TunnelGateway);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPBlock::MessageType::TunnelBuild);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPBlock::MessageType::TunnelBuildReply);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPBlock::MessageType::VariableTunnelBuild);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPBlock::MessageType::VariableTunnelBuildReply);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPBlock::MessageType::Reserved);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPBlock::MessageType::FutureReserved);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));
}
