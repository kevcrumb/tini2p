/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <catch2/catch.hpp>

#include "src/data/router/address.h"

using Address = tini2p::data::Address;

struct RouterAddressFixture
{
  RouterAddressFixture() : address(host, port) {}

  const std::string host{"13.37.4.2"};
  const std::uint16_t port{9111};
  Address address;
};

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress has a cost", "[address]")
{
  REQUIRE(address.cost == Address::DefaultCost);
}

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress has a transport", "[address]")
{
  using Catch::Matchers::Equals;

  const std::string ntcp2_str("ntcp2");

  const std::string transport_str(address.transport.begin(), address.transport.end());

  REQUIRE_THAT(transport_str, Equals(ntcp2_str));
}

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress serializes a valid address", "[address]")
{
  REQUIRE_NOTHROW(address.serialize());
}

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress deserializes a valid address", "[address]")
{
  REQUIRE_NOTHROW(address.serialize());
  REQUIRE_NOTHROW(address.deserialize());
}

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress fails to serialize invalid expiration", "[address]")
{
  // any non-zero expiration is invalid
  ++address.expiration;

  REQUIRE_THROWS(address.serialize());
}

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress fails to serialize invalid transport", "[address]")
{
  // invalidate the transport length
  address.transport.push_back(0x2B);

  REQUIRE_THROWS(address.serialize());

  address.transport.pop_back();

  // invalidate the transport name
  ++address.transport.front();

  REQUIRE_THROWS(address.serialize());
}

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress fails to deserialize invalid expiration", "[address]")
{
  REQUIRE_NOTHROW(address.serialize());

  // any non-zero expiration is invalid
  ++address.buffer[Address::ExpirationOffset];

  REQUIRE_THROWS(address.deserialize());

}

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress fails to deserialize invalid transport", "[address]")
{
  REQUIRE_NOTHROW(address.serialize());

  // invalidate the transport length
  ++address.buffer[Address::TransportOffset];

  REQUIRE_THROWS(address.deserialize());

  --address.buffer[Address::TransportOffset];

  // invalidate the transport name
  ++address.buffer[Address::TransportOffset + 1];

  REQUIRE_THROWS(address.deserialize());
}
