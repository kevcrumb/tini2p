/* Copyright (c) 2019, tini2p
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <catch2/catch.hpp>

#include "src/netdb/netdb.h"

using Catch::Matchers::Equals;
using vec = std::vector<std::uint8_t>;

using NetDB = tini2p::netdb::NetDB;

struct NetDBFixture
{
  NetDBFixture() : netdb(std::make_shared<tini2p::data::Info>()) {}

  NetDB netdb;
  std::unique_ptr<tini2p::data::DatabaseLookup> lookup_msg;
  std::unique_ptr<tini2p::data::DatabaseStore> store_msg;
  tini2p::crypto::Sha256::digest_t search_key, from_key;
  NetDB::lookup_reply_t lookup_ret;
};

TEST_CASE_METHOD(NetDBFixture, "NetDB handles DatabaseStore messages", "[netdb]")
{
  auto info_ptr = std::make_shared<tini2p::data::Info>();

  REQUIRE_NOTHROW(store_msg.reset(new tini2p::data::DatabaseStore(info_ptr)));
  REQUIRE_NOTHROW(netdb.HandleDatabaseStore(*store_msg));
  REQUIRE(netdb.floodfills().size() == 1);
  REQUIRE(*(netdb.floodfills().front()) == *info_ptr);

  auto ls2_ptr = std::make_shared<tini2p::data::LeaseSet2>();

  REQUIRE_NOTHROW(store_msg.reset(new tini2p::data::DatabaseStore(ls2_ptr)));
  REQUIRE_NOTHROW(netdb.HandleDatabaseStore(*store_msg));
  REQUIRE(netdb.lease_sets().size() == 1);
  REQUIRE(*(netdb.lease_sets().front()) == *ls2_ptr);
}

TEST_CASE_METHOD(NetDBFixture, "NetDB handles DatabaseLookup messages", "[netdb]")
{
  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(search_key));  // randomize search key, unrealistic
  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(from_key));  // randomize from key, unrealistic
  REQUIRE_NOTHROW(lookup_msg.reset(new tini2p::data::DatabaseLookup(search_key, from_key)));

  REQUIRE_NOTHROW(netdb.HandleDatabaseLookup(*lookup_msg, lookup_ret));

  // NetDB empty, should be no match
  REQUIRE(std::holds_alternative<tini2p::data::DatabaseSearchReply>(lookup_ret));
  REQUIRE(std::get<tini2p::data::DatabaseSearchReply>(lookup_ret).peer_keys().empty());
}

TEST_CASE_METHOD(NetDBFixture, "NetDB finds entries after local storage", "[netdb]")
{
  auto info_ptr = std::make_shared<tini2p::data::Info>();

  // store RouterInfo
  REQUIRE_NOTHROW(store_msg.reset(new tini2p::data::DatabaseStore(info_ptr)));
  REQUIRE_NOTHROW(netdb.HandleDatabaseStore(*store_msg));
  REQUIRE(netdb.floodfills().size() == 1);
  REQUIRE(*(netdb.floodfills().front()) == *info_ptr);

  auto ls2_ptr = std::make_shared<tini2p::data::LeaseSet2>();

  // store LeaseSet2
  REQUIRE_NOTHROW(store_msg.reset(new tini2p::data::DatabaseStore(ls2_ptr)));
  REQUIRE_NOTHROW(netdb.HandleDatabaseStore(*store_msg));
  REQUIRE(netdb.lease_sets().size() == 1);
  REQUIRE(*(netdb.lease_sets().front()) == *ls2_ptr);

  search_key = info_ptr->hash();
  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(from_key));

  // lookup RouterInfo entry
  REQUIRE_NOTHROW(lookup_msg.reset(new tini2p::data::DatabaseLookup(search_key, from_key)));
  REQUIRE_NOTHROW(netdb.HandleDatabaseLookup(*lookup_msg, lookup_ret));
  REQUIRE(std::holds_alternative<tini2p::data::DatabaseStore>(lookup_ret));

  // lookup LeaseSet2 entry
  search_key = ls2_ptr->hash();
  REQUIRE_NOTHROW(lookup_msg.reset(new tini2p::data::DatabaseLookup(search_key, from_key)));
  REQUIRE_NOTHROW(netdb.HandleDatabaseLookup(*lookup_msg, lookup_ret));
  REQUIRE(std::holds_alternative<tini2p::data::DatabaseStore>(lookup_ret));

  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(search_key));  // randomize search key, unrealistic
  REQUIRE_NOTHROW(lookup_msg.reset(new tini2p::data::DatabaseLookup(search_key, from_key)));
  REQUIRE_NOTHROW(netdb.HandleDatabaseLookup(*lookup_msg, lookup_ret));

  // should be no local match, and return a DatabaseSearchReply w/ one peer key
  REQUIRE(std::holds_alternative<tini2p::data::DatabaseSearchReply>(lookup_ret));
  REQUIRE(std::get<tini2p::data::DatabaseSearchReply>(lookup_ret).peer_keys().size() == 1);
}

TEST_CASE_METHOD(NetDBFixture, "NetDB returns at most default_num peer keys in a DatabaseSearchReply", "[netdb]")
{
  // add DefaultPeersLen + 1 RouterInfos to the NetDB
  const auto& default_peers = tini2p::under_cast(NetDB::DefaultPeersLen);
  const auto& total_peers = default_peers + 1;
  for (std::uint8_t i = 0; i < total_peers; ++i)
    {
      REQUIRE_NOTHROW(store_msg.reset(new tini2p::data::DatabaseStore(std::make_shared<tini2p::data::Info>())));
      REQUIRE_NOTHROW(netdb.HandleDatabaseStore(*store_msg));
    }

  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(search_key));  // randomize search key, unrealistic
  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(from_key));  // randomize from key, unrealistic
  REQUIRE_NOTHROW(lookup_msg.reset(new tini2p::data::DatabaseLookup(search_key, from_key)));
  REQUIRE_NOTHROW(netdb.HandleDatabaseLookup(*lookup_msg, lookup_ret));

  // should be no local match, and return a DatabaseSearchReply w/ one peer key
  REQUIRE(std::holds_alternative<tini2p::data::DatabaseSearchReply>(lookup_ret));
  REQUIRE(std::get<tini2p::data::DatabaseSearchReply>(lookup_ret).peer_keys().size() == default_peers);
}

TEST_CASE_METHOD(NetDBFixture, "NetDB finds closest peers based on lookup flag", "[netdb]")
{
  // store a floodfill peer
  const auto ff_peer = std::make_shared<tini2p::data::Info>();
  REQUIRE_NOTHROW(store_msg.reset(new tini2p::data::DatabaseStore(ff_peer)));
  REQUIRE_NOTHROW(netdb.HandleDatabaseStore(*store_msg));

  // store a non-floodfill peer
  auto nonff_peer = std::make_shared<tini2p::data::Info>();
  nonff_peer->options().add(std::string("caps"), std::string("OR"));

  REQUIRE_NOTHROW(store_msg.reset(new tini2p::data::DatabaseStore(nonff_peer)));
  REQUIRE_NOTHROW(netdb.HandleDatabaseStore(*store_msg));

  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(search_key));  // randomize search key, unrealistic

  NetDB::peers_t peers;
  // perform an Exploratory lookup, should only return non-ff peer
  REQUIRE_NOTHROW(peers = netdb.ClosestPeers(search_key, tini2p::data::DatabaseLookup::LookupFlags::Explore));

  for (const auto& peer : peers)
    REQUIRE(peer == nonff_peer->hash());

  // perform a non-Exploratory lookup, should return both stored peers
  REQUIRE_NOTHROW(peers = netdb.ClosestPeers(search_key, tini2p::data::DatabaseLookup::LookupFlags::Explore));

  for (const auto& peer : peers)
    {
      const bool has_both = peer == nonff_peer->hash() || peer == ff_peer->hash();
      REQUIRE(has_both);
    }
}
